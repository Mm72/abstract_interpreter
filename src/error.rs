//! This module defines the application-specific errors.

use std::error::Error;
use std::fmt;
use std::io;
use std::num::ParseIntError;

use crate::parser::Rule;

/// Short-hand alias for a result with error of type `ParserErr`.
pub type ParserRes<I> = Result<I, ParserErr>;

/// Error type which represents an error in the parsing of an input
/// program.
#[derive(Debug)]
pub enum ParserErr {
    InvalidConst(String, ParseIntError),
    InvalidVar(String, ParseIntError),
    Pest(pest::error::Error<Rule>),
    Io(io::Error),
}

impl Error for ParserErr {}

impl fmt::Display for ParserErr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::InvalidConst(s,e) => write!(f, "Failed to parse the string `{}` as an arithmetic constant. Constants are i32 integers and the conversion failed due to: {}", s, e),
            Self::InvalidVar(s,e) => write!(f, "Failed to parse the string `{}` as a variable id. Ids are u32 integers and the conversion failed due to: {}", s, e),
            Self::Pest(e) => write!(f, "Pest failed to parse the input due to: {}", e),
            Self::Io(e) => write!(f, "I/O error: {}", e),
        }
    }
}

impl From<pest::error::Error<Rule>> for ParserErr {
    fn from(other: pest::error::Error<Rule>) -> Self {
        ParserErr::Pest(other)
    }
}

impl From<io::Error> for ParserErr {
    fn from(other: io::Error) -> Self {
        ParserErr::Io(other)
    }
}

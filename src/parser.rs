//! This module is responsible for parsing the input program into a `Stat`.
//!
//! The parser is based on the [pest](https://pest.rs/) library.

use std::io::BufRead;

use crate::parser::Rule::*;
use pest::iterators::{Pair, Pairs};
use pest::prec_climber::Assoc::Left;
use pest::prec_climber::{Operator, PrecClimber};
use pest::Parser;

use crate::ast::{ArithExpr, BoolExpr, IntExt, Stat, VarId, MINF, PINF};
use crate::error::{ParserErr, ParserRes};

#[derive(Parser)]
#[grammar = "while.pest"]
struct WhileParser;

/// Parse an arithmetic expression.
fn parse_arith(expr: Pairs<Rule>) -> ParserRes<ArithExpr> {
    // Use the `PrecClimber` tool provided by pest to parse infix
    // expressions with precedence (e.g. multiplication over sum).
    let climber = PrecClimber::new(vec![
        Operator::new(arith_add, Left) | Operator::new(arith_sub, Left),
        Operator::new(arith_mul, Left) | Operator::new(arith_div, Left),
    ]);

    climber.climb(
        expr,
        // Base case.
        |pair: Pair<Rule>| match pair.as_rule() {
            Rule::arith_interval => {
                let mut it = pair.into_inner();
                let a = it
                    .next()
                    .expect("arith_interval rule must have 2 children (1st missing)")
                    .as_str()
                    .parse::<IntExt>()?;
                let b = it
                    .next()
                    .expect("arith_interval rule must have 2 children (2nd missing)")
                    .as_str()
                    .parse::<IntExt>()?;
                Ok(ArithExpr::Interval(a, b))
            }
            Rule::arith_int => {
                let i = pair.as_str().parse::<IntExt>()?;
                Ok(ArithExpr::Interval(i, i))
            }
            Rule::arith_neg => {
                Ok(ArithExpr::Interval(0.into(), 0.into()) - parse_arith(pair.into_inner())?)
            }
            Rule::arith_var => {
                let s = pair.into_inner().as_str();
                Ok(ArithExpr::Var(
                    s.parse::<VarId>()
                        .map_err(|e| ParserErr::InvalidVar(s.to_owned(), e))?,
                ))
            }
            Rule::arith_expr => parse_arith(pair.into_inner()),
            _ => panic!("Unreachable: {:?}", pair),
        },
        // Inductive case.
        |lhs: ParserRes<ArithExpr>, op: Pair<Rule>, rhs: ParserRes<ArithExpr>| match op.as_rule() {
            Rule::arith_add => Ok(lhs? + rhs?),
            Rule::arith_sub => Ok(lhs? - rhs?),
            Rule::arith_mul => Ok(lhs? * rhs?),
            Rule::arith_div => Ok(lhs? / rhs?),
            _ => unreachable!(),
        },
    )
}

/// Parse an arithmetic comparison expression.
///
/// Along with the boolean constants (true,false) those are the terms of
/// boolean expressions.
fn parse_bool_arith_term(mut term: Pairs<Rule>) -> ParserRes<BoolExpr> {
    let lfs = parse_arith(
        term.next()
            .expect("bool_arith rule must have 3 children (1st missing)")
            .into_inner(),
    )?;
    let op = term
        .next()
        .expect("bool_arith rule must have 3 children (2nd missing)");
    let rhs = parse_arith(
        term.next()
            .expect("bool_arith rule must have 3 children (3rd missing)")
            .into_inner(),
    )?;

    Ok(match op.as_rule() {
        Rule::bool_eq => BoolExpr::Eq(lfs, rhs),
        Rule::bool_neq => BoolExpr::Ne(lfs, rhs),
        Rule::bool_le => BoolExpr::Le(lfs, rhs),
        Rule::bool_leq => BoolExpr::Leq(lfs, rhs),
        Rule::bool_gt => BoolExpr::Gt(lfs, rhs),
        Rule::bool_ge => BoolExpr::Ge(lfs, rhs),
        _ => unreachable!(),
    })
}

/// Parse a boolean literal.
fn parse_bool_lit(lit: Pair<Rule>) -> ParserRes<BoolExpr> {
    if let Rule::bool_inv_term = lit.as_rule() {
        return Ok(BoolExpr::Not(Box::new(parse_bool(lit.into_inner())?)));
    }

    Ok(match lit.as_rule() {
        Rule::bool_arith_term => parse_bool_arith_term(lit.into_inner())?,
        Rule::bool_true => BoolExpr::Const(true),
        Rule::bool_false => BoolExpr::Const(false),
        Rule::bool_expr => parse_bool(lit.into_inner())?,
        _ => unreachable!(),
    })
}

/// Parse a boolean expression.
fn parse_bool(mut expr: Pairs<Rule>) -> ParserRes<BoolExpr> {
    let mut lit = parse_bool_lit(
        expr.next()
            .expect("boolean expressions must contain at least one literal"),
    )?;

    while let Some(op) = expr.next() {
        let rhs = parse_bool_lit(expr.next().expect("missing right lit"))?;

        lit = match op.as_rule() {
            Rule::bool_and => BoolExpr::And(Box::new(lit), Box::new(rhs)),
            Rule::bool_or => BoolExpr::Or(Box::new(lit), Box::new(rhs)),
            _ => unreachable!(),
        };
    }

    Ok(lit)
}

/// Parse an assignment statement.
fn parse_assign(assign: Pair<Rule>) -> ParserRes<Stat> {
    assert_eq!(assign.as_rule(), Rule::stat_assign);

    let mut it = assign.into_inner();
    let var = it.next().unwrap();
    let expr = it.next().unwrap();

    let var = match var.as_rule() {
        Rule::arith_var => var.into_inner().as_str().parse::<u32>().unwrap(),
        _ => unreachable!(),
    };

    Ok(Stat::Assign(var, parse_arith(expr.into_inner())?, 0))
}

/// Parse an if-then-else statement.
pub fn parse_if_else(ifelse: Pair<Rule>) -> ParserRes<Stat> {
    assert_eq!(ifelse.as_rule(), Rule::stat_if_else);

    let mut it = ifelse.into_inner();
    let guard = parse_bool(it.next().expect("missing guard in ifelse").into_inner())?;
    let block_if = parse_block(it.next().expect("missing if block in ifelse").into_inner())?;
    let block_else = parse_block(
        it.next()
            .expect("missing else block in ifelse")
            .into_inner(),
    )?;

    Ok(Stat::IfElse(
        guard,
        Box::new(block_if),
        Box::new(block_else),
        0,
    ))
}

/// Parse a while loop.
fn parse_while(wh: Pair<Rule>) -> ParserRes<Stat> {
    assert_eq!(wh.as_rule(), Rule::stat_while);

    let mut it = wh.into_inner();
    let guard = parse_bool(it.next().expect("missing guard in while").into_inner())?;
    let block = parse_block(it.next().expect("missing block in while").into_inner())?;

    Ok(Stat::While(guard, Box::new(block), 0, 0))
}

/// Parse a statement.
fn parse_stat(stat: Pair<Rule>) -> ParserRes<Stat> {
    Ok(match stat.as_rule() {
        Rule::stat_assign => parse_assign(stat)?,
        Rule::stat_skip => Stat::Skip(0),
        Rule::stat_if_else => parse_if_else(stat)?,
        Rule::stat_while => parse_while(stat)?,
        _ => unreachable!(),
    })
}

/// Parse a sequence of statements.
fn parse_block<'a>(mut block: impl Iterator<Item = Pair<'a, Rule>>) -> ParserRes<Stat> {
    let first = parse_stat(block.next().expect("empty block"))?;
    block.try_fold(first, |acc, next| {
        Ok(Stat::Concat(Box::new(acc), Box::new(parse_stat(next)?)))
    })
}

/// Read and parse a init value from a data buffer.
///
/// An *init value* is a value that can initialize a variable.
/// It can be one of the following:
///   * `top` meaning the top abstract element (i.e. any value).
///   * `i` meaning that the variable is initialized to an integer constant.
///   * `[a,b]` meaning that the variable is initialized to an interval.
pub fn parse_init_value(mut src: impl BufRead) -> ParserRes<(IntExt, IntExt)> {
    let mut buf = String::new();
    src.read_to_string(&mut buf)?;

    let pair = WhileParser::parse(Rule::init_value, &buf)?
        .take_while(|pair| pair.as_rule() != Rule::EOI)
        .next()
        .expect("No output from the parse");

    Ok(match pair.as_rule() {
        Rule::arith_interval => {
            let mut it = pair.into_inner();
            let a = it
                .next()
                .expect("arith_interval rule must have 2 children (1st missing)")
                .as_str()
                .parse::<IntExt>()?;
            let b = it
                .next()
                .expect("arith_interval rule must have 2 children (2nd missing)")
                .as_str()
                .parse::<IntExt>()?;
            (a, b)
        }
        Rule::init_int => {
            let i = pair.as_str().parse::<IntExt>()?;
            (i, i)
        }
        Rule::init_top => (MINF, PINF),
        _ => unreachable!(),
    })
}

/// Read and parse a program from a data buffer.
pub fn parse_src(mut src: impl BufRead) -> ParserRes<Stat> {
    let mut buf = String::new();
    src.read_to_string(&mut buf)?;

    let parse =
        WhileParser::parse(Rule::program, &buf)?.take_while(|pair| pair.as_rule() != Rule::EOI);

    parse_block(parse).map(|mut stat| {
        stat.set_labels();
        stat
    })
}

#[cfg(test)]
mod tests {
    use crate::ast::{ArithExpr, BoolExpr, IntExt, Stat, VarId, MINF, PINF};
    use crate::parser::Rule;
    use crate::parser::{parse_arith, parse_bool, parse_src, WhileParser};
    use crate::pest::Parser;

    // Short-hand utility to parse an `ArithExpr` from a string.
    fn pa(src: &str) -> ArithExpr {
        let parse = WhileParser::parse(Rule::arith_expr, src).unwrap();
        parse_arith(parse).unwrap()
    }

    // Short-hand utility to parse an `BoolExpr` from a string.
    fn pb(src: &str) -> BoolExpr {
        let parse = WhileParser::parse(Rule::bool_expr, src).unwrap();
        parse_bool(parse).unwrap()
    }

    // Short-hand utility to parse a program (`Stat`) from a string.
    fn pg(src: &str) -> Stat {
        parse_src(src.as_bytes()).unwrap()
    }

    // Compare two programs (`Stat`) ignoring the statement labels.
    fn cmp_pg(s1: Stat, s2: Stat) -> bool {
        match (s1, s2) {
            (Stat::Skip(_), Stat::Skip(_)) => true,
            (Stat::Assign(v1, a1, _), Stat::Assign(v2, a2, _)) => v1 == v2 && a1 == a2,
            (Stat::Concat(a1, b1), Stat::Concat(a2, b2)) => cmp_pg(*a1, *a2) && cmp_pg(*b1, *b2),
            (Stat::IfElse(g1, t1, e1, _), Stat::IfElse(g2, t2, e2, _)) => {
                g1 == g2 && cmp_pg(*t1, *t2) && cmp_pg(*e1, *e2)
            }
            (Stat::While(g1, t1, _, _), Stat::While(g2, t2, _, _)) => g1 == g2 && cmp_pg(*t1, *t2),
            _ => false,
        }
    }

    // Short-hand utilty to create a constant.
    fn c(n: impl Into<IntExt> + Copy) -> ArithExpr {
        ArithExpr::Interval(n.into(), n.into())
    }

    // Short-hand utility to create an interval.
    fn i(a: impl Into<IntExt>, b: impl Into<IntExt>) -> ArithExpr {
        ArithExpr::Interval(a.into(), b.into())
    }

    // Short-hand utilty to create a var.
    fn v(n: u32) -> ArithExpr {
        ArithExpr::Var(n)
    }

    // Short-hand utility to create a truth value.
    fn t(n: bool) -> BoolExpr {
        BoolExpr::Const(n)
    }

    // Short-hand utility to create the bool expr `a_1 == a_2` from
    // `a_1` and `a_2`.
    fn eq(a1: ArithExpr, a2: ArithExpr) -> BoolExpr {
        BoolExpr::Eq(a1, a2)
    }

    // Short-hand utility to create the bool expr `a_1 != a_2` from
    // `a_1` and `a_2`.
    fn ne(a1: ArithExpr, a2: ArithExpr) -> BoolExpr {
        BoolExpr::Ne(a1, a2)
    }

    // Short-hand utility to create the bool expr `a_1 < a_2` from
    // `a_1` and `a_2`.
    fn le(a1: ArithExpr, a2: ArithExpr) -> BoolExpr {
        BoolExpr::Le(a1, a2)
    }

    // Short-hand utility to create the bool expr `a_1 <= a_2` from
    // `a_1` and `a_2`.
    fn leq(a1: ArithExpr, a2: ArithExpr) -> BoolExpr {
        BoolExpr::Leq(a1, a2)
    }

    // Short-hand utility to create the bool expr `a_1 > a_2` from
    // `a_1` and `a_2`.
    fn gt(a1: ArithExpr, a2: ArithExpr) -> BoolExpr {
        BoolExpr::Gt(a1, a2)
    }

    // Short-hand utility to create the bool expr `a_1 >= a_2` from
    // `a_1` and `a_2`.
    fn ge(a1: ArithExpr, a2: ArithExpr) -> BoolExpr {
        BoolExpr::Ge(a1, a2)
    }

    // Short-hand utility to convert a list of statements
    // to a single chain of concats.
    fn cat(mut stats: Vec<Stat>) -> Stat {
        let first = stats.remove(0);
        stats.into_iter().fold(first, |acc, next| {
            Stat::Concat(Box::new(acc), Box::new(next))
        })
    }

    // Short-hand utility to create a ifelse statement.
    fn ie(guard: BoolExpr, then_block: Stat, else_block: Stat) -> Stat {
        Stat::IfElse(guard, Box::new(then_block), Box::new(else_block), 0)
    }

    // Short-hand utility to create a while statement.
    fn w(guard: BoolExpr, block: Stat) -> Stat {
        Stat::While(guard, Box::new(block), 0, 0)
    }

    // Short-hand utility to create an assignment statement.
    fn a(var: VarId, expr: ArithExpr) -> Stat {
        Stat::Assign(var, expr, 0)
    }

    // Short-hand utility to create a skip statement.
    fn s() -> Stat {
        Stat::Skip(0)
    }

    #[test]
    #[should_panic]
    fn arith_empty() {
        pa("");
    }

    #[test]
    fn arith_const_base() {
        assert_eq!(pa("1"), c(1));
    }

    #[test]
    fn arith_const_whitespaces() {
        assert_eq!(pa("    3    "), c(3));
    }

    #[test]
    fn arith_const_parens_1() {
        assert_eq!(pa("(5)"), c(5));
    }

    #[test]
    fn arith_const_parens_2() {
        assert_eq!(pa("(((5)))"), c(5));
    }

    #[test]
    fn arith_const_parens_3() {
        assert_eq!(pa("  (  (  (5)  ) ) "), c(5));
    }

    #[test]
    #[should_panic]
    fn arith_const_plus_sign() {
        pa("+1");
    }

    #[test]
    fn arith_const_minus_sign() {
        assert_eq!(pa("-1"), c(0) - c(1));
    }

    #[test]
    fn arith_const_still_not_too_large() {
        // 2^31 - 1
        assert_eq!(pa("2147483647"), c(2147483647));
    }

    #[test]
    #[should_panic]
    fn arith_const_too_large() {
        // 2^31
        pa("2147483648");
    }

    #[test]
    fn arith_intrv_1() {
        assert_eq!(pa("[1,2]"), i(1, 2));
    }

    #[test]
    fn arith_intrv_2() {
        assert_eq!(pa("[1, +inf] "), i(1, PINF));
    }

    #[test]
    fn arith_intrv_3() {
        assert_eq!(pa(" [ -inf , +inf ] "), i(MINF, PINF));
    }

    #[test]
    fn arith_var_base() {
        assert_eq!(pa("v1"), v(1));
    }

    #[test]
    fn arith_var_whitespaces() {
        assert_eq!(pa("    v1      "), v(1));
    }

    #[test]
    fn arith_var_parens_1() {
        assert_eq!(pa("(v1)"), v(1));
    }

    #[test]
    fn arith_var_parens_2() {
        assert_eq!(pa(" ( ( ( v100) )    )  "), v(100));
    }

    #[test]
    fn arith_comp_base() {
        assert_eq!(pa("1 + 1"), c(1) + c(1));
    }

    #[test]
    fn arith_comp_whitespaces() {
        assert_eq!(pa(" 1 + 1     - 100"), c(1) + c(1) - c(100));
    }

    #[test]
    fn arith_comp_order() {
        assert_eq!(pa("v1 + v2 * 10 - 123"), v(1) + (v(2) * c(10)) - c(123));
    }

    #[test]
    fn arith_comp_parens_1() {
        assert_eq!(pa("(v1 + v2) * v3"), (v(1) + v(2)) * v(3));
    }

    #[test]
    fn arith_comp_parens_2() {
        assert_eq!(pa("v1 * (v2 + v3)"), v(1) * (v(2) + v(3)));
    }

    #[test]
    fn arith_comp_parens_3() {
        assert_eq!(pa("(v1 + v2)"), v(1) + v(2));
    }

    #[test]
    fn arith_comp_parens_4() {
        assert_eq!(pa("(((v1 + v2)))"), v(1) + v(2));
    }

    #[test]
    fn arith_comp_parens_5() {
        assert_eq!(
            pa("((v1+v2)-1*(5*v3))*2  "),
            ((v(1) + v(2)) - c(1) * (c(5) * v(3))) * c(2)
        );
    }

    #[test]
    fn arith_comp_parens_6() {
        assert_eq!(
            pa(" (v1 - v2 / v3 * (v4 + v2)) "),
            v(1) - v(2) / v(3) * (v(4) + v(2))
        );
    }

    #[test]
    #[should_panic]
    fn bool_empty() {
        pb("");
    }

    #[test]
    fn bool_truth_base_1() {
        assert_eq!(pb("true"), t(true));
    }

    #[test]
    fn bool_truth_base_2() {
        assert_eq!(pb("false"), t(false));
    }

    #[test]
    fn bool_truth_whitespaces() {
        assert_eq!(pb("     false "), t(false));
    }

    #[test]
    fn bool_truth_parens_1() {
        assert_eq!(pb("(false)"), t(false));
    }

    #[test]
    fn bool_truth_parens_2() {
        assert_eq!(pb("((((true))))"), t(true));
    }

    #[test]
    fn bool_truth_parens_3() {
        assert_eq!(pb("  ( ( ((true)  )) ) "), t(true));
    }

    #[test]
    fn bool_lit_1() {
        assert_eq!(pb("v1 == v2"), eq(v(1), v(2)));
    }

    #[test]
    fn bool_lit_2() {
        assert_eq!(pb("v1 <= v2"), leq(v(1), v(2)));
    }

    #[test]
    fn bool_lit_3() {
        assert_eq!(pb("v1 + 10 == v2 * v3"), eq(v(1) + c(10), v(2) * v(3)));
    }

    #[test]
    fn bool_lit_4() {
        assert_eq!(pb("(v1 + 20) * v3 == v2"), eq((v(1) + c(20)) * v(3), v(2)));
    }

    #[test]
    fn bool_not_1() {
        assert_eq!(pb("!true"), !t(true));
    }

    #[test]
    fn bool_not_2() {
        assert_eq!(pb("! v1 == v2"), !eq(v(1), v(2)));
    }

    #[test]
    fn bool_not_3() {
        assert_eq!(pb("!!true"), !!t(true));
    }

    #[test]
    fn bool_not_4() {
        assert_eq!(pb("!(v1 == v2)"), !eq(v(1), v(2)));
    }

    #[test]
    fn bool_not_5() {
        assert_eq!(pb("  ! ( (  v1 == v2 ))"), !eq(v(1), v(2)));
    }

    #[test]
    fn bool_not_6() {
        assert_eq!(pb("!(!(v1 == v2))"), !!eq(v(1), v(2)));
    }

    #[test]
    fn bool_and_1() {
        assert_eq!(pb("true && true"), t(true) & t(true));
    }

    #[test]
    fn bool_and_2() {
        assert_eq!(
            pb("true && (true && false)"),
            t(true) & (t(true) & t(false))
        );
    }

    #[test]
    fn bool_and_3() {
        assert_eq!(pb(" ( (  true  && true)  )  "), t(true) & t(true));
    }

    #[test]
    fn bool_and_4() {
        assert_eq!(pb("v1 == 0 && v2 == 1"), eq(v(1), c(0)) & eq(v(2), c(1)));
    }

    #[test]
    fn bool_and_5() {
        assert_eq!(
            pb("v1 + 10 == 0 && v2 - 20 == 1"),
            eq(v(1) + c(10), c(0)) & eq(v(2) - c(20), c(1))
        );
    }

    #[test]
    fn bool_and_6() {
        assert_eq!(
            pb("!(v1 + 10 == 0) && v2 - 20 == 1"),
            !eq(v(1) + c(10), c(0)) & eq(v(2) - c(20), c(1))
        );
    }

    #[test]
    fn bool_and_7() {
        assert_eq!(
            pb("!(!(true && false) && v1 <= 0) "),
            !(!(t(true) & t(false)) & leq(v(1), c(0)))
        );
    }

    #[test]
    fn bool_or() {
        assert_eq!(pb("v1 == 0 || v1 == 1"), eq(v(1), c(0)) | eq(v(1), c(1)));
    }

    #[test]
    fn bool_neq() {
        assert_eq!(pb("v1 != v2"), ne(v(1), v(2)));
    }

    #[test]
    fn bool_le() {
        assert_eq!(pb("v1 < v2"), le(v(1), v(2)));
    }

    #[test]
    fn bool_gt() {
        assert_eq!(pb("v1 > v2"), gt(v(1), v(2)));
    }

    #[test]
    fn bool_ge() {
        assert_eq!(pb("v1 >= v2"), ge(v(1), v(2)));
    }

    #[test]
    #[should_panic]
    fn program_empty() {
        pg("");
    }

    #[test]
    fn program_skip() {
        assert!(cmp_pg(pg("skip\n"), s()));
    }

    #[test]
    fn program_assign_1() {
        assert!(cmp_pg(
            pg("v2 := (v1 + 5) * 2\n"),
            a(2, (v(1) + c(5)) * c(2))
        ));
    }

    #[test]
    fn program_assign_2() {
        assert!(cmp_pg(
            pg("v2 :=/* cmt */ (v1 + 5) * 2 // cmt2 \n"),
            a(2, (v(1) + c(5)) * c(2))
        ));
    }

    #[test]
    fn program_concat() {
        assert!(cmp_pg(
            pg("skip\n\
                skip\n\
                v1 := 4\n\
                skip\n\
                v2 := 10\n"),
            cat(vec![s(), s(), a(1, c(4)), s(), a(2, c(10)),])
        ));
    }

    #[test]
    fn program_ifelse_single() {
        assert!(cmp_pg(
            pg("if true\n\
                \tv1 := 2\n\
                else
                \tv2 := 4+5\n\
                "),
            ie(t(true), a(1, c(2)), a(2, c(4) + c(5)))
        ));
    }

    #[test]
    fn program_ifelse_multiple() {
        assert!(cmp_pg(
            pg("if true\n\
                \tv1 := 2\n\
                \tv4 := 3\n\
                else
                \tv2 := 4+5\n\
                \tskip
                "),
            ie(
                t(true),
                cat(vec![a(1, c(2)), a(4, c(3))]),
                cat(vec![a(2, c(4) + c(5)), s()])
            )
        ));
    }

    #[test]
    fn program_while() {
        assert!(cmp_pg(
            pg("while true\n\
                \tv1 := 2\n\
                \tv2 := 4+5\n"),
            w(t(true), cat(vec![a(1, c(2)), a(2, c(4) + c(5))]))
        ));
    }

    #[test]
    fn program_while_nested() {
        assert!(cmp_pg(
            pg("while true\n\
                \twhile false\n\
                \t\twhile v1 == 2\n\
                \t\t\tv1 := 2\n"),
            w(t(true), w(t(false), w(eq(v(1), c(2)), a(1, c(2)))))
        ));
    }

    #[test]
    fn program_full_line_comments() {
        assert!(cmp_pg(
            pg("while true\n\
                \t// Comment line\n\
                \tv1 := 2\n"),
            w(t(true), a(1, c(2)))
        ));
    }

    #[test]
    fn program_empty_lines() {
        assert!(cmp_pg(
            pg("v1 := 1\n\
                \n\n\n\
                skip\n\
                \n\n"),
            cat(vec![a(1, c(1)), s()])
        ));
    }
}

//! Sign is an abstract integer domain which represents the sign of
//! a set of integers.
//!
//! It is a finite size domain with the following elements:
//!   * **Bottom:** represents an empty set of integers.
//!   * **Top:** represents the entire set of integers.
//!   * **Plus:** represents the positive integers.
//!   * **Zero:** represents the singleton zero.
//!   * **Minus:** represents the negative integers.

use std::cmp::{Ordering, PartialOrd};
use std::fmt;
use std::ops::{Add, Div, Mul, Sub};

use super::{IntDomain, IntDomainTr, VarStore};
use crate::ast::{ArithExpr, IntExt};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum SignDomain {
    Top,
    Plus,
    Minus,
    Zero,
    Bottom,
}

impl Add for SignDomain {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        match (self, other) {
            (Self::Bottom, _) => Self::Bottom,
            (_, Self::Bottom) => Self::Bottom,
            (Self::Top, _) => Self::Top,
            (_, Self::Top) => Self::Top,
            (Self::Plus, Self::Plus) => Self::Plus,
            (Self::Plus, Self::Zero) => Self::Plus,
            (Self::Plus, Self::Minus) => Self::Top,
            (Self::Minus, Self::Plus) => Self::Top,
            (Self::Minus, Self::Zero) => Self::Minus,
            (Self::Minus, Self::Minus) => Self::Minus,
            (Self::Zero, Self::Plus) => Self::Plus,
            (Self::Zero, Self::Zero) => Self::Zero,
            (Self::Zero, Self::Minus) => Self::Minus,
        }
    }
}

impl Sub for SignDomain {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        match (self, other) {
            (Self::Bottom, _) => Self::Bottom,
            (_, Self::Bottom) => Self::Bottom,
            (Self::Top, _) => Self::Top,
            (_, Self::Top) => Self::Top,
            (Self::Plus, Self::Plus) => Self::Top,
            (Self::Plus, Self::Zero) => Self::Plus,
            (Self::Plus, Self::Minus) => Self::Plus,
            (Self::Zero, Self::Plus) => Self::Minus,
            (Self::Zero, Self::Zero) => Self::Zero,
            (Self::Zero, Self::Minus) => Self::Plus,
            (Self::Minus, Self::Plus) => Self::Minus,
            (Self::Minus, Self::Zero) => Self::Minus,
            (Self::Minus, Self::Minus) => Self::Top,
        }
    }
}

impl Mul for SignDomain {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        match (self, other) {
            (Self::Bottom, _) => Self::Bottom,
            (_, Self::Bottom) => Self::Bottom,
            (Self::Zero, _) => Self::Zero,
            (_, Self::Zero) => Self::Zero,
            (Self::Top, _) => Self::Top,
            (_, Self::Top) => Self::Top,
            (Self::Plus, Self::Plus) => Self::Plus,
            (Self::Plus, Self::Minus) => Self::Minus,
            (Self::Minus, Self::Plus) => Self::Minus,
            (Self::Minus, Self::Minus) => Self::Plus,
        }
    }
}

impl Div for SignDomain {
    type Output = Self;

    fn div(self, other: Self) -> Self {
        match (self, other) {
            (Self::Bottom, _) => Self::Bottom,
            (_, Self::Bottom) => Self::Bottom,
            (_, Self::Zero) => Self::Bottom,
            (Self::Zero, _) => Self::Zero,
            (Self::Top, _) => Self::Top,
            (_, Self::Top) => Self::Top,
            (Self::Plus, Self::Plus) => Self::Plus,
            (Self::Plus, Self::Minus) => Self::Top,
            (Self::Minus, Self::Plus) => Self::Top,
            (Self::Minus, Self::Minus) => Self::Minus,
        }
    }
}

impl PartialOrd for SignDomain {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (Self::Top, Self::Top) => Some(Ordering::Equal),
            (Self::Top, _) => Some(Ordering::Greater),
            (_, Self::Top) => Some(Ordering::Less),
            (Self::Bottom, Self::Bottom) => Some(Ordering::Equal),
            (Self::Bottom, _) => Some(Ordering::Less),
            (_, Self::Bottom) => Some(Ordering::Greater),
            (Self::Plus, Self::Plus) => Some(Ordering::Equal),
            (Self::Plus, Self::Zero) => None,
            (Self::Plus, Self::Minus) => None,
            (Self::Zero, Self::Plus) => None,
            (Self::Zero, Self::Zero) => Some(Ordering::Equal),
            (Self::Zero, Self::Minus) => None,
            (Self::Minus, Self::Plus) => None,
            (Self::Minus, Self::Zero) => None,
            (Self::Minus, Self::Minus) => Some(Ordering::Equal),
        }
    }
}

impl fmt::Display for SignDomain {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Bottom => write!(f, "⊥"),
            Self::Top => write!(f, "⊤"),
            Self::Plus => write!(f, ">0"),
            Self::Zero => write!(f, "=0"),
            Self::Minus => write!(f, "<0"),
        }
    }
}

impl IntDomainTr for SignDomain {
    fn top() -> Self {
        Self::Top
    }

    fn bottom() -> Self {
        Self::Bottom
    }

    fn join(self, other: Self) -> Self {
        self.partial_cmp(&other)
            .map(|ord| match ord {
                Ordering::Less => other,
                Ordering::Equal => self,
                Ordering::Greater => self,
            })
            .unwrap_or(Self::Top)
    }

    fn meet(self, other: Self) -> Self {
        self.partial_cmp(&other)
            .map(|ord| match ord {
                Ordering::Less => self,
                Ordering::Equal => self,
                Ordering::Greater => other,
            })
            .unwrap_or(Self::Bottom)
    }

    fn widen(self, other: Self) -> Self {
        self.join(other)
    }

    fn from_interval(a: IntExt, b: IntExt) -> Self {
        if a > b {
            return Self::Bottom;
        }

        if a == IntExt::Int(0) && b == IntExt::Int(0) {
            return Self::Zero;
        }

        if a > IntExt::Int(0) {
            return Self::Plus;
        }

        if b < IntExt::Int(0) {
            return Self::Minus;
        }

        Self::Top
    }

    fn filter_eq(a1: &ArithExpr, a2: &ArithExpr, mut vars: VarStore) -> VarStore {
        let d1 = vars.eval::<SignDomain>(a1);
        let d2 = vars.eval::<SignDomain>(a2);

        if match (d1, d2) {
            (_, Self::Bottom) => true,
            (Self::Bottom, _) => true,
            (Self::Plus, Self::Zero) => true,
            (Self::Plus, Self::Minus) => true,
            (Self::Zero, Self::Plus) => true,
            (Self::Zero, Self::Minus) => true,
            (Self::Minus, Self::Plus) => true,
            (Self::Minus, Self::Zero) => true,
            _ => false,
        } {
            VarStore::bottom::<Self>()
        } else {
            let d = d1.meet(d2);

            match (a1, a2) {
                (ArithExpr::Var(v1), ArithExpr::Var(v2)) => {
                    vars[*v1] = d.cover();
                    vars[*v2] = d.cover();
                }
                (ArithExpr::Var(v), _) => vars[*v] = d.cover(),
                (_, ArithExpr::Var(v)) => vars[*v] = d.cover(),
                _ => (),
            }

            vars
        }
    }

    fn filter_ne(a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore {
        let d1 = vars.eval::<Self>(a1);
        let d2 = vars.eval::<Self>(a2);

        let bottom = match (d1, d2) {
            (_, Self::Bottom) => true,
            (Self::Bottom, _) => true,
            (Self::Zero, Self::Zero) => true,
            _ => false,
        };

        if bottom {
            VarStore::bottom::<Self>()
        } else {
            vars
        }
    }

    fn filter_le(a1: &ArithExpr, a2: &ArithExpr, mut vars: VarStore) -> VarStore {
        let d1 = vars.eval::<Self>(a1);
        let d2 = vars.eval::<Self>(a2);

        let bottom = match (d1, d2) {
            (_, Self::Bottom) => true,
            (Self::Bottom, _) => true,
            (Self::Plus, Self::Zero) => true,
            (Self::Plus, Self::Minus) => true,
            (Self::Zero, Self::Minus) => true,
            _ => false,
        };

        if bottom {
            VarStore::bottom::<Self>()
        } else {
            if let ArithExpr::Var(v) = a1 {
                if d2 == Self::Zero {
                    vars[*v] = Self::Minus.cover();
                }
            }

            vars
        }
    }

    fn filter_leq(a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore {
        let d1 = vars.eval::<Self>(a1);
        let d2 = vars.eval::<Self>(a2);

        let bottom = match (d1, d2) {
            (_, Self::Bottom) => true,
            (Self::Bottom, _) => true,
            (Self::Plus, Self::Zero) => true,
            (Self::Plus, Self::Minus) => true,
            (Self::Zero, Self::Minus) => true,
            _ => false,
        };

        if bottom {
            VarStore::bottom::<Self>()
        } else {
            vars
        }
    }

    fn filter_gt(a1: &ArithExpr, a2: &ArithExpr, mut vars: VarStore) -> VarStore {
        let d1 = vars.eval::<Self>(a1);
        let d2 = vars.eval::<Self>(a2);

        let bottom = match (d1, d2) {
            (_, Self::Bottom) => true,
            (Self::Bottom, _) => true,
            (Self::Minus, Self::Zero) => true,
            (Self::Minus, Self::Plus) => true,
            (Self::Zero, Self::Plus) => true,
            _ => false,
        };

        if bottom {
            VarStore::bottom::<Self>()
        } else {
            if let ArithExpr::Var(v) = a1 {
                if d2 == Self::Zero {
                    vars[*v] = Self::Plus.cover();
                }
            }

            vars
        }
    }

    fn filter_ge(a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore {
        let d1 = vars.eval::<Self>(a1);
        let d2 = vars.eval::<Self>(a2);

        let bottom = match (d1, d2) {
            (_, Self::Bottom) => true,
            (Self::Bottom, _) => true,
            (Self::Minus, Self::Zero) => true,
            (Self::Minus, Self::Plus) => true,
            (Self::Zero, Self::Plus) => true,
            _ => false,
        };

        if bottom {
            VarStore::bottom::<Self>()
        } else {
            vars
        }
    }

    fn uncover(d: IntDomain) -> Self {
        match d {
            IntDomain::Sign(d) => d,
            _ => panic!("SignDomain::uncover domain kind mismatch"),
        }
    }

    fn cover(self) -> IntDomain {
        IntDomain::Sign(self)
    }
}

#[cfg(test)]
mod tests {
    use crate::domain::sign::SignDomain;

    #[test]
    fn add_1() {
        assert_eq!(SignDomain::Plus + SignDomain::Plus, SignDomain::Plus);
    }

    #[test]
    fn add_2() {
        assert_eq!(SignDomain::Bottom + SignDomain::Top, SignDomain::Bottom);
        assert_eq!(SignDomain::Top + SignDomain::Bottom, SignDomain::Bottom);
    }

    #[test]
    fn add_3() {
        assert_eq!(SignDomain::Zero + SignDomain::Minus, SignDomain::Minus);
    }

    #[test]
    fn ord_1() {
        assert!(SignDomain::Bottom < SignDomain::Plus);
    }
}

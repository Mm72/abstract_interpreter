//! Const is an integer abstract domain which represents constant values.
//!
//! As a consequence the elements in the domain are:
//!   * **Bottom:** representing an empty set of integers.
//!   * **Top:** representing the entire set of integers.
//!   * **Int:** representing a set with just one value (hence a
//!   constant).

use std::cmp::{Ordering, PartialOrd};
use std::fmt;
use std::ops::{Add, Div, Mul, Sub};

use super::{IntDomain, IntDomainTr, VarStore};
use crate::ast::{ArithExpr, IntExt, IntTp};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ConstDomain {
    Int(IntTp),
    Top,
    Bottom,
}

impl ConstDomain {
    fn arithmetic(d1: Self, d2: Self, mut f: impl FnMut(IntTp, IntTp) -> Option<IntTp>) -> Self {
        match (d1, d2) {
            (Self::Bottom, _) => Self::Bottom,
            (_, Self::Bottom) => Self::Bottom,
            (Self::Top, _) => Self::Top,
            (_, Self::Top) => Self::Top,
            (Self::Int(i1), Self::Int(i2)) => match f(i1, i2) {
                Some(res) => Self::Int(res),
                None => Self::Bottom,
            },
        }
    }

    fn filter(
        a1: &ArithExpr,
        a2: &ArithExpr,
        vars: VarStore,
        mut f: impl FnMut(IntTp, IntTp) -> bool,
    ) -> VarStore {
        let d1 = vars.eval::<ConstDomain>(a1);
        let d2 = vars.eval::<ConstDomain>(a2);

        if match (d1, d2) {
            (ConstDomain::Bottom, _) => true,
            (_, ConstDomain::Bottom) => true,
            (ConstDomain::Int(i1), ConstDomain::Int(i2)) => !f(i1, i2),
            _ => false,
        } {
            VarStore::bottom::<ConstDomain>()
        } else {
            vars
        }
    }
}

impl Add for ConstDomain {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self::arithmetic(self, other, |x, y| Some(x + y))
    }
}

impl Sub for ConstDomain {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self::arithmetic(self, other, |x, y| Some(x - y))
    }
}

impl Mul for ConstDomain {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        Self::arithmetic(self, other, |x, y| Some(x * y))
    }
}

impl Div for ConstDomain {
    type Output = Self;

    fn div(self, other: Self) -> Self {
        Self::arithmetic(self, other, |x, y| x.checked_div(y))
    }
}

impl PartialOrd for ConstDomain {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (Self::Bottom, Self::Bottom) => Some(Ordering::Equal),
            (Self::Bottom, _) => Some(Ordering::Less),
            (_, Self::Bottom) => Some(Ordering::Greater),
            (Self::Top, Self::Top) => Some(Ordering::Equal),
            (Self::Top, _) => Some(Ordering::Greater),
            (_, Self::Top) => Some(Ordering::Less),
            (Self::Int(x), Self::Int(y)) => {
                if x == y {
                    Some(Ordering::Equal)
                } else {
                    None
                }
            }
        }
    }
}

impl fmt::Display for ConstDomain {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Bottom => write!(f, "⊥"),
            Self::Top => write!(f, "⊤"),
            Self::Int(i) => write!(f, "{}", i),
        }
    }
}

impl IntDomainTr for ConstDomain {
    fn top() -> Self {
        Self::Top
    }

    fn bottom() -> Self {
        Self::Bottom
    }

    fn join(self, other: Self) -> Self {
        self.partial_cmp(&other)
            .map(|ord| match ord {
                Ordering::Less => other,
                Ordering::Equal => self,
                Ordering::Greater => self,
            })
            .unwrap_or(Self::Top)
    }

    fn meet(self, other: Self) -> Self {
        self.partial_cmp(&other)
            .map(|ord| match ord {
                Ordering::Less => self,
                Ordering::Equal => self,
                Ordering::Greater => other,
            })
            .unwrap_or(Self::Bottom)
    }

    fn widen(self, other: Self) -> Self {
        self.join(other)
    }

    fn from_interval(a: IntExt, b: IntExt) -> Self {
        match (a, b) {
            (IntExt::Int(a), IntExt::Int(b)) => {
                if a == b {
                    Self::Int(a)
                } else {
                    Self::Top
                }
            }
            _ => Self::Top,
        }
    }

    fn filter_eq(a1: &ArithExpr, a2: &ArithExpr, mut vars: VarStore) -> VarStore {
        let d1 = vars.eval::<ConstDomain>(a1);
        let d2 = vars.eval::<ConstDomain>(a2);

        if match (d1, d2) {
            (ConstDomain::Bottom, _) => true,
            (_, ConstDomain::Bottom) => true,
            (ConstDomain::Int(i1), ConstDomain::Int(i2)) => i1 != i2,
            _ => false,
        } {
            VarStore::bottom::<ConstDomain>()
        } else if let ArithExpr::Var(v) = a1 {
            if d1 == ConstDomain::Top {
                assert_eq!(d1.cover(), vars[*v]);
                *vars.0.get_mut(v).expect("missing var") = d1.meet(d2).cover();
                vars
            } else {
                vars
            }
        } else if let ArithExpr::Var(v) = a2 {
            if d2 == ConstDomain::Top {
                assert_eq!(d2.cover(), vars[*v]);
                *vars.0.get_mut(v).expect("missing var") = d1.meet(d2).cover();
                vars
            } else {
                vars
            }
        } else {
            vars
        }
    }

    fn filter_ne(a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore {
        Self::filter(a1, a2, vars, |x, y| x != y)
    }

    fn filter_le(a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore {
        Self::filter(a1, a2, vars, |x, y| x < y)
    }

    fn filter_leq(a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore {
        Self::filter(a1, a2, vars, |x, y| x <= y)
    }

    fn filter_gt(a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore {
        Self::filter(a1, a2, vars, |x, y| x > y)
    }

    fn filter_ge(a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore {
        Self::filter(a1, a2, vars, |x, y| x >= y)
    }

    fn uncover(d: IntDomain) -> Self {
        match d {
            IntDomain::Const(d) => d,
            _ => panic!("uncover Const called on wrong int domain"),
        }
    }

    fn cover(self) -> IntDomain {
        IntDomain::Const(self)
    }
}

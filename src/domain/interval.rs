//! Interval is an abstract integer domain that represents intervals of
//! integer numbers.
//!
//! As a consequence the elements of this domain are:
//!   * **Bottom:** which represents a set with no integers in it.
//!   * **Interval:** which represents an interval of integers. The bounds
//!   can be also infinite, to represent unbounded intervals. Notice
//!   that the top is represented as an interval with bounds equal to `-inf`
//!   and `+inf`.

use std::cmp::{Ordering, PartialOrd};
use std::fmt;
use std::ops::{Add, Div, Mul, Neg, Sub};

use super::{IntDomain, IntDomainTr, VarStore};
use crate::ast::{ArithExpr, IntExt, IntTp, MINF, PINF};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct IntervalDomain {
    a: IntExt,
    b: IntExt,
}

impl IntervalDomain {
    fn smash_bottom(self) -> Self {
        if self.a > self.b {
            IntervalDomain { a: PINF, b: MINF }
        } else {
            self
        }
    }

    fn is_bottom(self) -> bool {
        self == IntervalDomain { a: PINF, b: MINF }
    }

    fn get_const(self) -> Option<IntTp> {
        match (self.a, self.b) {
            (IntExt::Int(a), IntExt::Int(b)) if a == b => Some(a),
            _ => None,
        }
    }
}

impl Add for IntervalDomain {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        let a = match (self.a, other.a) {
            (IntExt::PlusInf, IntExt::MinusInf) => PINF,
            (IntExt::MinusInf, IntExt::PlusInf) => PINF,
            _ => self.a + other.a,
        };

        let b = match (self.a, other.a) {
            (IntExt::PlusInf, IntExt::MinusInf) => MINF,
            (IntExt::MinusInf, IntExt::PlusInf) => MINF,
            _ => self.b + other.b,
        };

        IntervalDomain { a, b }
    }
}

impl Neg for IntervalDomain {
    type Output = Self;

    fn neg(self) -> Self {
        if self.is_bottom() {
            self
        } else {
            IntervalDomain {
                a: -self.b,
                b: -self.a,
            }
        }
    }
}

impl Sub for IntervalDomain {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        self + (-other)
    }
}

impl Mul for IntervalDomain {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        if self.is_bottom() || other.is_bottom() {
            return Self::bottom();
        }

        let parts = [
            self.a * other.a,
            self.a * other.b,
            self.b * other.a,
            self.b * other.b,
        ];

        IntervalDomain {
            a: parts.iter().copied().min().unwrap(),
            b: parts.iter().copied().max().unwrap(),
        }
    }
}

impl Div for IntervalDomain {
    type Output = Self;

    fn div(self, mut other: Self) -> Self {
        if self.is_bottom() || other.is_bottom() {
            Self::bottom()
        } else if other.a == IntExt::Int(0) && other.b == IntExt::Int(0) {
            Self::bottom()
        } else if other.a >= IntExt::Int(0) {
            if other.a == IntExt::Int(0) {
                other.a = IntExt::Int(1);
            }

            let parts = [
                self.a / other.a,
                self.a / other.b,
                self.b / other.a,
                self.b / other.b,
            ];

            IntervalDomain {
                a: parts.iter().copied().min().unwrap(),
                b: parts.iter().copied().max().unwrap(),
            }
        } else if other.b <= IntExt::Int(0) {
            (-self) / (-other)
        } else {
            let x = self
                / IntervalDomain {
                    a: other.a,
                    b: IntExt::Int(-1),
                };

            let y = self
                / IntervalDomain {
                    a: IntExt::Int(1),
                    b: other.b,
                };

            x.join(y)
        }
    }
}

impl PartialOrd for IntervalDomain {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let a = self.a.partial_cmp(&other.a);
        let b = self.b.partial_cmp(&other.b);

        match (a, b) {
            (Some(Ordering::Equal), Some(Ordering::Equal)) => Some(Ordering::Equal),
            (Some(Ordering::Greater), Some(Ordering::Less)) => Some(Ordering::Less),
            (Some(Ordering::Greater), Some(Ordering::Equal)) => Some(Ordering::Less),
            (Some(Ordering::Equal), Some(Ordering::Less)) => Some(Ordering::Less),
            (Some(Ordering::Less), Some(Ordering::Greater)) => Some(Ordering::Greater),
            (Some(Ordering::Less), Some(Ordering::Equal)) => Some(Ordering::Greater),
            (Some(Ordering::Equal), Some(Ordering::Greater)) => Some(Ordering::Greater),
            _ => None,
        }
    }
}

impl fmt::Display for IntervalDomain {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.is_bottom() {
            write!(f, "⊥")
        } else {
            write!(f, "[{}, {}]", self.a, self.b)
        }
    }
}

impl IntDomainTr for IntervalDomain {
    fn top() -> Self {
        IntervalDomain { a: MINF, b: PINF }
    }

    fn bottom() -> Self {
        IntervalDomain { a: PINF, b: MINF }
    }

    fn join(self, other: Self) -> Self {
        let a = self.a.min(other.a);
        let b = self.b.max(other.b);
        IntervalDomain { a, b }
    }

    fn meet(self, other: Self) -> Self {
        let a = self.a.max(other.a);
        let b = self.b.min(other.b);
        Self::smash_bottom(IntervalDomain { a, b })
    }

    fn widen(mut self, other: Self) -> Self {
        if self.a > other.a {
            self.a = MINF;
        }

        if self.b < other.b {
            self.b = PINF;
        }

        self
    }

    fn narrow(mut self, other: Self) -> Self {
        if self.a == MINF {
            self.a = other.a;
        }

        if self.b == PINF {
            self.b = other.b;
        }

        self
    }

    fn filter_eq(a1: &ArithExpr, a2: &ArithExpr, mut vars: VarStore) -> VarStore {
        let d1 = vars.eval::<Self>(a1);
        let d2 = vars.eval::<Self>(a2);
        let d = d1.meet(d2);

        if d.is_bottom() {
            VarStore::bottom::<Self>()
        } else {
            match (a1, a2) {
                (ArithExpr::Var(v1), ArithExpr::Var(v2)) => {
                    let d = d1.meet(d2);
                    vars[*v1] = d.cover();
                    vars[*v2] = d.cover();
                }
                (ArithExpr::Var(v), _) => vars[*v] = d.cover(),
                (_, ArithExpr::Var(v)) => vars[*v] = d.cover(),
                _ => (),
            }

            vars
        }
    }

    fn filter_ne(a1: &ArithExpr, a2: &ArithExpr, mut vars: VarStore) -> VarStore {
        let mut d1 = vars.eval::<Self>(a1);
        let mut d2 = vars.eval::<Self>(a2);

        fn restrict_on_const(x: &mut IntervalDomain, y: &mut IntervalDomain) {
            if let Some(c) = y.get_const() {
                if x.a == y.a {
                    x.a = IntExt::Int(c + 1);
                } else if x.b == y.a {
                    x.b = IntExt::Int(c - 1);
                }

                *x = x.smash_bottom();
            }
        }

        restrict_on_const(&mut d1, &mut d2);
        restrict_on_const(&mut d2, &mut d1);

        if d1.is_bottom() || d2.is_bottom() {
            VarStore::bottom::<Self>()
        } else {
            match (a1, a2) {
                (ArithExpr::Var(v1), ArithExpr::Var(v2)) => {
                    vars[*v1] = d1.cover();
                    vars[*v2] = d2.cover();
                }
                (ArithExpr::Var(v), _) => vars[*v] = d1.cover(),
                (_, ArithExpr::Var(v)) => vars[*v] = d2.cover(),
                _ => (),
            }

            vars
        }
    }

    fn filter_le(a1: &ArithExpr, a2: &ArithExpr, mut vars: VarStore) -> VarStore {
        let d1 = vars.eval::<Self>(a1);
        let d2 = vars.eval::<Self>(a2);

        if d1.a >= d2.b {
            VarStore::bottom::<Self>()
        } else {
            let x = IntervalDomain {
                a: d1.a,
                b: d1.b.min(d2.b - IntExt::Int(1)),
            };
            let y = IntervalDomain {
                a: d1.a.max(d2.a + IntExt::Int(1)),
                b: d2.b,
            };

            match (a1, a2) {
                (ArithExpr::Var(v1), ArithExpr::Var(v2)) => {
                    vars[*v1] = x.cover();
                    vars[*v2] = y.cover();
                }
                (ArithExpr::Var(v), _) => vars[*v] = x.cover(),
                (_, ArithExpr::Var(v)) => vars[*v] = y.cover(),
                _ => (),
            }

            vars
        }
    }

    fn filter_leq(a1: &ArithExpr, a2: &ArithExpr, mut vars: VarStore) -> VarStore {
        let d1 = vars.eval::<Self>(a1);
        let d2 = vars.eval::<Self>(a2);

        if d1.a > d2.b {
            VarStore::bottom::<Self>()
        } else {
            let x = IntervalDomain {
                a: d1.a,
                b: d1.b.min(d2.b),
            };
            let y = IntervalDomain {
                a: d1.a.max(d2.a),
                b: d2.b,
            };

            match (a1, a2) {
                (ArithExpr::Var(v1), ArithExpr::Var(v2)) => {
                    vars[*v1] = x.cover();
                    vars[*v2] = y.cover();
                }
                (ArithExpr::Var(v), _) => vars[*v] = x.cover(),
                (_, ArithExpr::Var(v)) => vars[*v] = y.cover(),
                _ => (),
            }

            vars
        }
    }

    fn filter_gt(a1: &ArithExpr, a2: &ArithExpr, mut vars: VarStore) -> VarStore {
        let d1 = vars.eval::<Self>(a1);
        let d2 = vars.eval::<Self>(a2);

        if d1.b <= d2.a {
            VarStore::bottom::<Self>()
        } else {
            let x = IntervalDomain {
                a: d1.a.max(d2.a + IntExt::Int(1)),
                b: d1.b,
            };
            let y = IntervalDomain {
                a: d2.a,
                b: d2.b.min(d1.b - IntExt::Int(1)),
            };

            match (a1, a2) {
                (ArithExpr::Var(v1), ArithExpr::Var(v2)) => {
                    vars[*v1] = x.cover();
                    vars[*v2] = y.cover();
                }
                (ArithExpr::Var(v), _) => vars[*v] = x.cover(),
                (_, ArithExpr::Var(v)) => vars[*v] = y.cover(),
                _ => (),
            }

            vars
        }
    }

    fn filter_ge(a1: &ArithExpr, a2: &ArithExpr, mut vars: VarStore) -> VarStore {
        let d1 = vars.eval::<Self>(a1);
        let d2 = vars.eval::<Self>(a2);

        if d1.b < d2.a {
            VarStore::bottom::<Self>()
        } else {
            let x = IntervalDomain {
                a: d1.a.max(d2.a),
                b: d1.b,
            };
            let y = IntervalDomain {
                a: d2.a,
                b: d2.b.min(d1.b),
            };

            match (a1, a2) {
                (ArithExpr::Var(v1), ArithExpr::Var(v2)) => {
                    vars[*v1] = x.cover();
                    vars[*v2] = y.cover();
                }
                (ArithExpr::Var(v), _) => vars[*v] = x.cover(),
                (_, ArithExpr::Var(v)) => vars[*v] = y.cover(),
                _ => (),
            }

            vars
        }
    }

    fn from_interval(a: IntExt, b: IntExt) -> Self {
        Self::smash_bottom(IntervalDomain { a, b })
    }

    fn uncover(d: IntDomain) -> Self {
        match d {
            IntDomain::Interval(d) => d,
            _ => panic!("InvervalDomain::uncover domain kind mismatch"),
        }
    }

    fn cover(self) -> IntDomain {
        IntDomain::Interval(self)
    }
}

#[cfg(test)]
mod tests {
    use crate::ast::IntExt;
    use crate::domain::interval::IntervalDomain;

    #[test]
    fn add_1() {
        let x = IntervalDomain {
            a: IntExt::Int(-5),
            b: IntExt::Int(7),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-7),
            b: IntExt::Int(4),
        };
        assert_eq!(
            x + y,
            IntervalDomain {
                a: IntExt::Int(-12),
                b: IntExt::Int(11)
            }
        );
    }

    #[test]
    fn sub_1() {
        let x = IntervalDomain {
            a: IntExt::Int(-5),
            b: IntExt::Int(7),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-7),
            b: IntExt::Int(4),
        };
        assert_eq!(
            x - y,
            IntervalDomain {
                a: IntExt::Int(-9),
                b: IntExt::Int(14)
            }
        );
    }

    #[test]
    fn mul_1() {
        let x = IntervalDomain {
            a: IntExt::Int(-5),
            b: IntExt::Int(7),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-7),
            b: IntExt::Int(4),
        };
        assert_eq!(
            x * y,
            IntervalDomain {
                a: IntExt::Int(-49),
                b: IntExt::Int(35)
            }
        );
    }

    #[test]
    fn div_1() {
        let x = IntervalDomain {
            a: IntExt::Int(-5),
            b: IntExt::Int(7),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-7),
            b: IntExt::Int(4),
        };
        assert_eq!(
            x / y,
            IntervalDomain {
                a: IntExt::Int(-7),
                b: IntExt::Int(7)
            }
        );
    }

    #[test]
    fn add_2() {
        let x = IntervalDomain {
            a: IntExt::Int(-1),
            b: IntExt::Int(4),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-13),
            b: IntExt::Int(-1),
        };
        assert_eq!(
            x + y,
            IntervalDomain {
                a: IntExt::Int(-14),
                b: IntExt::Int(3)
            }
        );
    }

    #[test]
    fn sub_2() {
        let x = IntervalDomain {
            a: IntExt::Int(-1),
            b: IntExt::Int(4),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-13),
            b: IntExt::Int(-1),
        };
        assert_eq!(
            x - y,
            IntervalDomain {
                a: IntExt::Int(0),
                b: IntExt::Int(17)
            }
        );
    }

    #[test]
    fn mul_2() {
        let x = IntervalDomain {
            a: IntExt::Int(-1),
            b: IntExt::Int(4),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-13),
            b: IntExt::Int(-1),
        };
        assert_eq!(
            x * y,
            IntervalDomain {
                a: IntExt::Int(-52),
                b: IntExt::Int(13)
            }
        );
    }

    #[test]
    fn div_2() {
        let x = IntervalDomain {
            a: IntExt::Int(-1),
            b: IntExt::Int(4),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-13),
            b: IntExt::Int(-1),
        };
        assert_eq!(
            x / y,
            IntervalDomain {
                a: IntExt::Int(-4),
                b: IntExt::Int(1)
            }
        );
    }

    #[test]
    fn add_3() {
        let x = IntervalDomain {
            a: IntExt::Int(-4),
            b: IntExt::Int(14),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-15),
            b: IntExt::Int(8),
        };
        assert_eq!(
            x + y,
            IntervalDomain {
                a: IntExt::Int(-19),
                b: IntExt::Int(22)
            }
        );
    }

    #[test]
    fn sub_3() {
        let x = IntervalDomain {
            a: IntExt::Int(-4),
            b: IntExt::Int(14),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-15),
            b: IntExt::Int(8),
        };
        assert_eq!(
            x - y,
            IntervalDomain {
                a: IntExt::Int(-12),
                b: IntExt::Int(29)
            }
        );
    }

    #[test]
    fn mul_3() {
        let x = IntervalDomain {
            a: IntExt::Int(-4),
            b: IntExt::Int(14),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-15),
            b: IntExt::Int(8),
        };
        assert_eq!(
            x * y,
            IntervalDomain {
                a: IntExt::Int(-210),
                b: IntExt::Int(112)
            }
        );
    }

    #[test]
    fn div_3() {
        let x = IntervalDomain {
            a: IntExt::Int(-4),
            b: IntExt::Int(14),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-15),
            b: IntExt::Int(8),
        };
        assert_eq!(
            x / y,
            IntervalDomain {
                a: IntExt::Int(-14),
                b: IntExt::Int(14)
            }
        );
    }

    #[test]
    fn add_4() {
        let x = IntervalDomain {
            a: IntExt::Int(-11),
            b: IntExt::Int(-6),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-9),
            b: IntExt::Int(8),
        };
        assert_eq!(
            x + y,
            IntervalDomain {
                a: IntExt::Int(-20),
                b: IntExt::Int(2)
            }
        );
    }

    #[test]
    fn sub_4() {
        let x = IntervalDomain {
            a: IntExt::Int(-11),
            b: IntExt::Int(-6),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-9),
            b: IntExt::Int(8),
        };
        assert_eq!(
            x - y,
            IntervalDomain {
                a: IntExt::Int(-19),
                b: IntExt::Int(3)
            }
        );
    }

    #[test]
    fn mul_4() {
        let x = IntervalDomain {
            a: IntExt::Int(-11),
            b: IntExt::Int(-6),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-9),
            b: IntExt::Int(8),
        };
        assert_eq!(
            x * y,
            IntervalDomain {
                a: IntExt::Int(-88),
                b: IntExt::Int(99)
            }
        );
    }

    #[test]
    fn div_4() {
        let x = IntervalDomain {
            a: IntExt::Int(-11),
            b: IntExt::Int(-6),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-9),
            b: IntExt::Int(8),
        };
        assert_eq!(
            x / y,
            IntervalDomain {
                a: IntExt::Int(-11),
                b: IntExt::Int(11)
            }
        );
    }

    #[test]
    fn add_5() {
        let x = IntervalDomain {
            a: IntExt::Int(-13),
            b: IntExt::Int(-6),
        };
        let y = IntervalDomain {
            a: IntExt::Int(7),
            b: IntExt::Int(7),
        };
        assert_eq!(
            x + y,
            IntervalDomain {
                a: IntExt::Int(-6),
                b: IntExt::Int(1)
            }
        );
    }

    #[test]
    fn sub_5() {
        let x = IntervalDomain {
            a: IntExt::Int(-13),
            b: IntExt::Int(-6),
        };
        let y = IntervalDomain {
            a: IntExt::Int(7),
            b: IntExt::Int(7),
        };
        assert_eq!(
            x - y,
            IntervalDomain {
                a: IntExt::Int(-20),
                b: IntExt::Int(-13)
            }
        );
    }

    #[test]
    fn mul_5() {
        let x = IntervalDomain {
            a: IntExt::Int(-13),
            b: IntExt::Int(-6),
        };
        let y = IntervalDomain {
            a: IntExt::Int(7),
            b: IntExt::Int(7),
        };
        assert_eq!(
            x * y,
            IntervalDomain {
                a: IntExt::Int(-91),
                b: IntExt::Int(-42)
            }
        );
    }

    #[test]
    fn div_5() {
        let x = IntervalDomain {
            a: IntExt::Int(-13),
            b: IntExt::Int(-6),
        };
        let y = IntervalDomain {
            a: IntExt::Int(7),
            b: IntExt::Int(7),
        };
        assert_eq!(
            x / y,
            IntervalDomain {
                a: IntExt::Int(-1),
                b: IntExt::Int(0)
            }
        );
    }

    #[test]
    fn add_6() {
        let x = IntervalDomain {
            a: IntExt::Int(-14),
            b: IntExt::Int(-11),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-13),
            b: IntExt::Int(-5),
        };
        assert_eq!(
            x + y,
            IntervalDomain {
                a: IntExt::Int(-27),
                b: IntExt::Int(-16)
            }
        );
    }

    #[test]
    fn sub_6() {
        let x = IntervalDomain {
            a: IntExt::Int(-14),
            b: IntExt::Int(-11),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-13),
            b: IntExt::Int(-5),
        };
        assert_eq!(
            x - y,
            IntervalDomain {
                a: IntExt::Int(-9),
                b: IntExt::Int(2)
            }
        );
    }

    #[test]
    fn mul_6() {
        let x = IntervalDomain {
            a: IntExt::Int(-14),
            b: IntExt::Int(-11),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-13),
            b: IntExt::Int(-5),
        };
        assert_eq!(
            x * y,
            IntervalDomain {
                a: IntExt::Int(55),
                b: IntExt::Int(182)
            }
        );
    }

    #[test]
    fn div_6() {
        let x = IntervalDomain {
            a: IntExt::Int(-14),
            b: IntExt::Int(-11),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-13),
            b: IntExt::Int(-5),
        };
        assert_eq!(
            x / y,
            IntervalDomain {
                a: IntExt::Int(0),
                b: IntExt::Int(2)
            }
        );
    }

    #[test]
    fn add_7() {
        let x = IntervalDomain {
            a: IntExt::Int(8),
            b: IntExt::Int(14),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-8),
            b: IntExt::Int(-8),
        };
        assert_eq!(
            x + y,
            IntervalDomain {
                a: IntExt::Int(0),
                b: IntExt::Int(6)
            }
        );
    }

    #[test]
    fn sub_7() {
        let x = IntervalDomain {
            a: IntExt::Int(8),
            b: IntExt::Int(14),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-8),
            b: IntExt::Int(-8),
        };
        assert_eq!(
            x - y,
            IntervalDomain {
                a: IntExt::Int(16),
                b: IntExt::Int(22)
            }
        );
    }

    #[test]
    fn mul_7() {
        let x = IntervalDomain {
            a: IntExt::Int(8),
            b: IntExt::Int(14),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-8),
            b: IntExt::Int(-8),
        };
        assert_eq!(
            x * y,
            IntervalDomain {
                a: IntExt::Int(-112),
                b: IntExt::Int(-64)
            }
        );
    }

    #[test]
    fn div_7() {
        let x = IntervalDomain {
            a: IntExt::Int(8),
            b: IntExt::Int(14),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-8),
            b: IntExt::Int(-8),
        };
        assert_eq!(
            x / y,
            IntervalDomain {
                a: IntExt::Int(-1),
                b: IntExt::Int(-1)
            }
        );
    }

    #[test]
    fn add_8() {
        let x = IntervalDomain {
            a: IntExt::Int(-6),
            b: IntExt::Int(12),
        };
        let y = IntervalDomain {
            a: IntExt::Int(4),
            b: IntExt::Int(13),
        };
        assert_eq!(
            x + y,
            IntervalDomain {
                a: IntExt::Int(-2),
                b: IntExt::Int(25)
            }
        );
    }

    #[test]
    fn sub_8() {
        let x = IntervalDomain {
            a: IntExt::Int(-6),
            b: IntExt::Int(12),
        };
        let y = IntervalDomain {
            a: IntExt::Int(4),
            b: IntExt::Int(13),
        };
        assert_eq!(
            x - y,
            IntervalDomain {
                a: IntExt::Int(-19),
                b: IntExt::Int(8)
            }
        );
    }

    #[test]
    fn mul_8() {
        let x = IntervalDomain {
            a: IntExt::Int(-6),
            b: IntExt::Int(12),
        };
        let y = IntervalDomain {
            a: IntExt::Int(4),
            b: IntExt::Int(13),
        };
        assert_eq!(
            x * y,
            IntervalDomain {
                a: IntExt::Int(-78),
                b: IntExt::Int(156)
            }
        );
    }

    #[test]
    fn div_8() {
        let x = IntervalDomain {
            a: IntExt::Int(-6),
            b: IntExt::Int(12),
        };
        let y = IntervalDomain {
            a: IntExt::Int(4),
            b: IntExt::Int(13),
        };
        assert_eq!(
            x / y,
            IntervalDomain {
                a: IntExt::Int(-1),
                b: IntExt::Int(3)
            }
        );
    }

    #[test]
    fn add_9() {
        let x = IntervalDomain {
            a: IntExt::Int(-14),
            b: IntExt::Int(3),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-4),
            b: IntExt::Int(6),
        };
        assert_eq!(
            x + y,
            IntervalDomain {
                a: IntExt::Int(-18),
                b: IntExt::Int(9)
            }
        );
    }

    #[test]
    fn sub_9() {
        let x = IntervalDomain {
            a: IntExt::Int(-14),
            b: IntExt::Int(3),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-4),
            b: IntExt::Int(6),
        };
        assert_eq!(
            x - y,
            IntervalDomain {
                a: IntExt::Int(-20),
                b: IntExt::Int(7)
            }
        );
    }

    #[test]
    fn mul_9() {
        let x = IntervalDomain {
            a: IntExt::Int(-14),
            b: IntExt::Int(3),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-4),
            b: IntExt::Int(6),
        };
        assert_eq!(
            x * y,
            IntervalDomain {
                a: IntExt::Int(-84),
                b: IntExt::Int(56)
            }
        );
    }

    #[test]
    fn div_9() {
        let x = IntervalDomain {
            a: IntExt::Int(-14),
            b: IntExt::Int(3),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-4),
            b: IntExt::Int(6),
        };
        assert_eq!(
            x / y,
            IntervalDomain {
                a: IntExt::Int(-14),
                b: IntExt::Int(14)
            }
        );
    }

    #[test]
    fn add_10() {
        let x = IntervalDomain {
            a: IntExt::Int(-3),
            b: IntExt::Int(9),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-3),
            b: IntExt::Int(1),
        };
        assert_eq!(
            x + y,
            IntervalDomain {
                a: IntExt::Int(-6),
                b: IntExt::Int(10)
            }
        );
    }

    #[test]
    fn sub_10() {
        let x = IntervalDomain {
            a: IntExt::Int(-3),
            b: IntExt::Int(9),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-3),
            b: IntExt::Int(1),
        };
        assert_eq!(
            x - y,
            IntervalDomain {
                a: IntExt::Int(-4),
                b: IntExt::Int(12)
            }
        );
    }

    #[test]
    fn mul_10() {
        let x = IntervalDomain {
            a: IntExt::Int(-3),
            b: IntExt::Int(9),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-3),
            b: IntExt::Int(1),
        };
        assert_eq!(
            x * y,
            IntervalDomain {
                a: IntExt::Int(-27),
                b: IntExt::Int(9)
            }
        );
    }

    #[test]
    fn div_10() {
        let x = IntervalDomain {
            a: IntExt::Int(-3),
            b: IntExt::Int(9),
        };
        let y = IntervalDomain {
            a: IntExt::Int(-3),
            b: IntExt::Int(1),
        };
        assert_eq!(
            x / y,
            IntervalDomain {
                a: IntExt::Int(-9),
                b: IntExt::Int(9)
            }
        );
    }
}

//! Introduction
//! ============
//!
//! This module contains the implementation of the abstract domains.
//!
//! The abstract domains can be either *state* or *integer* domains depending
//! on the abstracted set. Indeed a concrete program invariant is a set of states,
//! i.e. a mapping from variables to values. On the other hand a concrete invariant
//! for a variable is a set of integers.
//!
//! The former must implement the `StateDomainTr` trait, while the latter the
//! `IntDomainTr` one.
//!
//! The user should use the `StateDomain` struct when he aims to access a
//! generic state domain, or `IntDomain` for a integer one. It would've been possible
//! also to use trait objects of type `&dyn StateDomainTr` and `&dyn IntDomainTr`, but
//! that would've made the definitions of such traits much more complex.
//!
//! Relationality
//! =============
//!
//! Abstract domains of states can be *relational* if they track information
//! which involves multiple variables at the same time. For example a linear equality
//! such as `x + y = 5` is relational information.
//!
//! On the contrary, *non-relational* domains collect information of each variable
//! independently from the others. For examples `x in [0,6]`.
//!
//! This kind of domains can be easily defined starting from an abstract domain over
//! integers: you just need to consider a vector which stores for each variable an
//! integer domain, for example assuming a integer domain that tracks the sign,
//! `[x > 0, y < 0]` is a possible state domain. This constuction is implemented by
//! the `NonRela` struct.
//!
//! Domains
//! =======
//!
//! For know only non-relational domains are supported, but the infrastructure
//! can also accomodate relational ones.
//!
//! The following domains are supported (each one is implemented a submodule, refer to that
//! for more information):
//!   * Sign
//!   * Constant
//!   * Interval

use std::cmp::Ordering;
use std::collections::HashMap;
use std::fmt;
use std::fmt::Display;
use std::hash::Hash;
use std::ops::{Add, Div, Index, IndexMut, Mul, Sub};

pub mod cnst;
pub mod interval;
pub mod sign;

use cnst::ConstDomain;
use interval::IntervalDomain;
use sign::SignDomain;

use crate::ast::{ArithExpr, BoolExpr, IntExt, VarId};
use crate::VARS;

/// List of all the abstract domains supported by this application.
pub const DOMAINS: [StateDomainKind; 3] = [
    StateDomainKind::Sign,
    StateDomainKind::Const,
    StateDomainKind::Interval,
];

/// Trait implemented by abstract domains over set of integers.
///
/// Three main categories of functions must be implemented:
///   * **Domain topology:** it must be possible to, e.g. get the top element
///   (`top()`), compute the join between two elements, widen, etc.
///   * **Arithmentic:** the abstract evaluation of arithmetic expressions
///   is computed in an inductive fashion over the expression's tree. The domain
///   must implement the `Add`,`Sub`, etc traits to allow composition at
///   inner nodes of the tree.
///   * **Filtering:** domains must implement filtering functions for
///   literals of boolean expressions.
pub trait IntDomainTr:
    Add<Output = Self>
    + Div<Output = Self>
    + Mul<Output = Self>
    + Sub<Output = Self>
    + PartialEq
    + PartialOrd
    + Clone
    + Display
{
    /// Returns the top element of the abstract domain.
    fn top() -> Self;

    /// Returns the bottom element of the abstract domain.
    fn bottom() -> Self;

    /// Returns the join between `self` and `other`.
    fn join(self, other: Self) -> Self;

    /// Returns the meet between `self` and `other`.
    fn meet(self, other: Self) -> Self;

    /// Returns the widening of `self` with `other`.
    fn widen(self, other: Self) -> Self;

    /// Returns the narrowing of `self` with `other`.
    fn narrow(self, _other: Self) -> Self {
        self
    }

    /// Given a variables store, filter the states that satisfy `a1 == a2`.
    fn filter_eq(a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore;

    /// Given a variables store, filter the states that satisfy `a1 != a2`.
    fn filter_ne(a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore;

    /// Given a variables store, filter the states that satisfy `a1 < a2`.
    fn filter_le(a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore;

    /// Given a variables store, filter the states that satisfy `a1 <= a2`.
    fn filter_leq(a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore;

    /// Given a variables store, filter the states that satisfy `a1 > a2`.
    fn filter_gt(a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore;

    /// Given a variables store, filter the states that satisfy `a1 >= a2`.
    fn filter_ge(a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore;

    /// Create the abstract element that best approximates the concrete
    /// interval `[a,b]`.
    fn from_interval(a: IntExt, b: IntExt) -> Self;

    /// Given a `IntDomain`, if it wraps an integer domain of kind `Self`
    /// then returns the wrapped value.
    ///
    /// Panics if the kind is not `Self`.
    fn uncover(d: IntDomain) -> Self;

    /// Wrap `self` in an `IntDomain`.
    fn cover(self) -> IntDomain;
}

/// This type wraps all the supported integer domains.
///
/// To construct it, use the `cover(self)` function over the concrete
/// domain.
#[derive(Debug, Clone)]
pub enum IntDomain {
    Sign(SignDomain),
    Const(ConstDomain),
    Interval(IntervalDomain),
}

impl IntDomain {
    /// Returns the category of this domain.
    pub fn kind(&self) -> IntDomainKind {
        match self {
            Self::Sign(_) => IntDomainKind::Sign,
            Self::Const(_) => IntDomainKind::Const,
            Self::Interval(_) => IntDomainKind::Interval,
        }
    }

    /// Returns the join between `self` and `other`.
    pub fn join(self, other: Self) -> Self {
        match (self, other) {
            (Self::Sign(d1), Self::Sign(d2)) => d1.join(d2).cover(),
            (Self::Const(d1), Self::Const(d2)) => d1.join(d2).cover(),
            (Self::Interval(d1), Self::Interval(d2)) => d1.join(d2).cover(),
            _ => panic!("IntDomain::join domain kind mismatch"),
        }
    }

    /// Returns the meet between `self` and `other`.
    pub fn meet(self, other: Self) -> Self {
        match (self, other) {
            (Self::Sign(d1), Self::Sign(d2)) => d1.meet(d2).cover(),
            (Self::Const(d1), Self::Const(d2)) => d1.meet(d2).cover(),
            (Self::Interval(d1), Self::Interval(d2)) => d1.meet(d2).cover(),
            _ => panic!("IntDomain::meet domain kind mismatch"),
        }
    }

    /// Returns the widening of `self` with `other`.
    pub fn widen(self, other: Self) -> Self {
        match (self, other) {
            (Self::Sign(d1), Self::Sign(d2)) => d1.widen(d2).cover(),
            (Self::Const(d1), Self::Const(d2)) => d1.widen(d2).cover(),
            (Self::Interval(d1), Self::Interval(d2)) => d1.widen(d2).cover(),
            _ => panic!("IntDomain::widen domain kind mismatch"),
        }
    }

    /// Returns the narrowing of `self` with `other`.
    pub fn narrow(self, other: Self) -> Self {
        match (self, other) {
            (Self::Sign(d1), Self::Sign(d2)) => d1.narrow(d2).cover(),
            (Self::Const(d1), Self::Const(d2)) => d1.narrow(d2).cover(),
            (Self::Interval(d1), Self::Interval(d2)) => d1.narrow(d2).cover(),
            _ => panic!("IntDomain::narrow domain kind mismatch"),
        }
    }
}

impl Add for IntDomain {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        match (self, other) {
            (Self::Sign(d1), Self::Sign(d2)) => (d1 + d2).cover(),
            (Self::Const(d1), Self::Const(d2)) => (d1 + d2).cover(),
            (Self::Interval(d1), Self::Interval(d2)) => (d1 + d2).cover(),
            _ => panic!("IntDomain::add domain kind mismatch"),
        }
    }
}

impl Sub for IntDomain {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        match (self, other) {
            (Self::Sign(d1), Self::Sign(d2)) => (d1 - d2).cover(),
            (Self::Const(d1), Self::Const(d2)) => (d1 - d2).cover(),
            (Self::Interval(d1), Self::Interval(d2)) => (d1 - d2).cover(),
            _ => panic!("IntDomain::sub domain kind mismatch"),
        }
    }
}

impl Mul for IntDomain {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        match (self, other) {
            (Self::Sign(d1), Self::Sign(d2)) => (d1 * d2).cover(),
            (Self::Const(d1), Self::Const(d2)) => (d1 * d2).cover(),
            (Self::Interval(d1), Self::Interval(d2)) => (d1 * d2).cover(),
            _ => panic!("IntDomain::mul domain kind mismatch"),
        }
    }
}

impl Div for IntDomain {
    type Output = Self;

    fn div(self, other: Self) -> Self {
        match (self, other) {
            (Self::Sign(d1), Self::Sign(d2)) => (d1 / d2).cover(),
            (Self::Const(d1), Self::Const(d2)) => (d1 / d2).cover(),
            (Self::Interval(d1), Self::Interval(d2)) => (d1 / d2).cover(),
            _ => panic!("IntDomain::div domain kind mismatch"),
        }
    }
}

impl PartialEq for IntDomain {
    fn eq(&self, y: &Self) -> bool {
        match (self, y) {
            (Self::Sign(d1), Self::Sign(d2)) => d1 == d2,
            (Self::Const(d1), Self::Const(d2)) => d1 == d2,
            (Self::Interval(d1), Self::Interval(d2)) => d1 == d2,
            _ => panic!("IntDomain::eq domain kind mismatch"),
        }
    }
}

impl PartialOrd for IntDomain {
    fn partial_cmp(&self, y: &Self) -> Option<Ordering> {
        match (self, y) {
            (Self::Sign(d1), Self::Sign(d2)) => d1.partial_cmp(d2),
            (Self::Const(d1), Self::Const(d2)) => d1.partial_cmp(d2),
            (Self::Interval(d1), Self::Interval(d2)) => d1.partial_cmp(d2),
            _ => panic!("IntDomain::partial_cmp domain kind mismatch"),
        }
    }
}

impl fmt::Display for IntDomain {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Sign(d) => d.fmt(f),
            Self::Const(d) => d.fmt(f),
            Self::Interval(d) => d.fmt(f),
        }
    }
}

/// Kind of integer domain.
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub enum IntDomainKind {
    Sign,
    Const,
    Interval,
}

impl IntDomainKind {
    /// Return the top element of `self` kind of domain.
    #[allow(dead_code)]
    pub fn top(&self) -> IntDomain {
        match self {
            Self::Sign => SignDomain::top().cover(),
            Self::Const => ConstDomain::top().cover(),
            Self::Interval => IntervalDomain::top().cover(),
        }
    }

    /// Return the bottom element of `self` kind of domain.
    pub fn bottom(&self) -> IntDomain {
        match self {
            Self::Sign => SignDomain::bottom().cover(),
            Self::Const => ConstDomain::bottom().cover(),
            Self::Interval => IntervalDomain::bottom().cover(),
        }
    }

    /// Return the best abstract element of kind `self` which contains
    /// the interval `[a,b]`.
    #[allow(dead_code)]
    pub fn from_interval(&self, a: IntExt, b: IntExt) -> IntDomain {
        match self {
            Self::Sign => SignDomain::from_interval(a, b).cover(),
            Self::Const => ConstDomain::from_interval(a, b).cover(),
            Self::Interval => IntervalDomain::from_interval(a, b).cover(),
        }
    }

    /// Given a variables store, filter the states that satisfy `a1 == a2`.
    pub fn filter_eq(&self, a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore {
        match self {
            Self::Sign => SignDomain::filter_eq(a1, a2, vars),
            Self::Const => ConstDomain::filter_eq(a1, a2, vars),
            Self::Interval => IntervalDomain::filter_eq(a1, a2, vars),
        }
    }

    /// Given a variables store, filter the states that satisfy `a1 != a2`.
    pub fn filter_ne(&self, a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore {
        match self {
            Self::Sign => SignDomain::filter_ne(a1, a2, vars),
            Self::Const => ConstDomain::filter_ne(a1, a2, vars),
            Self::Interval => IntervalDomain::filter_ne(a1, a2, vars),
        }
    }

    /// Given a variables store, filter the states that satisfy `a1 < a2`.
    pub fn filter_le(&self, a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore {
        match self {
            Self::Sign => SignDomain::filter_le(a1, a2, vars),
            Self::Const => ConstDomain::filter_le(a1, a2, vars),
            Self::Interval => IntervalDomain::filter_le(a1, a2, vars),
        }
    }

    /// Given a variables store, filter the states that satisfy `a1 <= a2`.
    pub fn filter_leq(&self, a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore {
        match self {
            Self::Sign => SignDomain::filter_leq(a1, a2, vars),
            Self::Const => ConstDomain::filter_leq(a1, a2, vars),
            Self::Interval => IntervalDomain::filter_leq(a1, a2, vars),
        }
    }

    /// Given a variables store, filter the states that satisfy `a1 > a2`.
    pub fn filter_gt(&self, a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore {
        match self {
            Self::Sign => SignDomain::filter_gt(a1, a2, vars),
            Self::Const => ConstDomain::filter_gt(a1, a2, vars),
            Self::Interval => IntervalDomain::filter_gt(a1, a2, vars),
        }
    }

    /// Given a variables store, filter the states that satisfy `a1 >= a2`.
    pub fn filter_ge(&self, a1: &ArithExpr, a2: &ArithExpr, vars: VarStore) -> VarStore {
        match self {
            Self::Sign => SignDomain::filter_ge(a1, a2, vars),
            Self::Const => ConstDomain::filter_ge(a1, a2, vars),
            Self::Interval => IntervalDomain::filter_ge(a1, a2, vars),
        }
    }
}

/// Trait implemented by abstract domains over set of states.
///
/// Three main categories of functions must be implemented:
///   * **Domain topology:** it must be possible to, e.g. compute the join
///   between two elements, widen, leq, etc.
///   * **Filtering and assignment:** domains must implement filtering and
///   assignment functions that are used by the interpreter to compute
///   abstract elements through the control flow graph.
pub trait StateDomainTr: PartialEq + Clone {
    /// Returns the join between `self` and `other`.
    fn join(self, other: Self) -> Self;

    /// Returns the meet between `self` and `other`.
    fn meet(self, other: Self) -> Self;

    /// Returns the widening of `self` to `other`.
    fn widen(self, other: Self) -> Self;

    /// Returns the narrowing of `self` to `other`.
    fn narrow(self, _other: Self) -> Self {
        self
    }

    /// Returns `true` if `self` is less or equal than `other`.
    fn leq(&self, other: &Self) -> bool;

    /// Compute an abstract state that corresponds to the assignment of the
    /// expression `expr` to the variable `var`, starting from the state
    /// `self`.
    fn assign(self, var: VarId, expr: &ArithExpr) -> Self;

    /// Filter the abstract state according to the boolean guard `b`.
    fn guard(self, b: &BoolExpr) -> Self;

    /// Wrap self in an `StateDomain`.
    fn cover(self) -> StateDomain;

    /// Given a `StateDomain`, if it wraps a domain of kind `Self`
    /// then returns the wrapped value.
    ///
    /// Panics if the kind is not Self.
    fn uncover(d: StateDomain) -> Self;
}

/// This type wraps all the supported state domains.
///
/// To construct it, use the cover(self) function over the concrete domain.
#[derive(Debug, Clone)]
pub enum StateDomain {
    Sign(NonRela),
    Const(NonRela),
    Interval(NonRela),
}

impl StateDomain {
    /// Returns the join between `self` and `other`.
    pub fn join(self, other: Self) -> Self {
        match (self, other) {
            (Self::Sign(d1), Self::Sign(d2)) => d1.join(d2).cover(),
            (Self::Const(d1), Self::Const(d2)) => d1.join(d2).cover(),
            (Self::Interval(d1), Self::Interval(d2)) => d1.join(d2).cover(),
            _ => panic!("StateDomain::join domain kind mismatch"),
        }
    }

    /// Returns the meet between `self` and `other`.
    pub fn meet(self, other: Self) -> Self {
        match (self, other) {
            (Self::Sign(d1), Self::Sign(d2)) => d1.meet(d2).cover(),
            (Self::Const(d1), Self::Const(d2)) => d1.meet(d2).cover(),
            (Self::Interval(d1), Self::Interval(d2)) => d1.meet(d2).cover(),
            _ => panic!("StateDomain::meet domain kind mismatch"),
        }
    }

    /// Returns the widening of `self` with `other`.
    pub fn widen(self, other: Self) -> Self {
        match (self, other) {
            (Self::Sign(d1), Self::Sign(d2)) => d1.widen(d2).cover(),
            (Self::Const(d1), Self::Const(d2)) => d1.widen(d2).cover(),
            (Self::Interval(d1), Self::Interval(d2)) => d1.widen(d2).cover(),
            _ => panic!("StateDomain::widen domain kind mismatch"),
        }
    }

    /// Returns the narrowing of `self` with `other`.
    pub fn narrow(self, other: Self) -> Self {
        match (self, other) {
            (Self::Sign(d1), Self::Sign(d2)) => d1.narrow(d2).cover(),
            (Self::Const(d1), Self::Const(d2)) => d1.narrow(d2).cover(),
            (Self::Interval(d1), Self::Interval(d2)) => d1.narrow(d2).cover(),
            _ => panic!("StateDomain::narrow domain kind mismatch"),
        }
    }

    /// Returns true if self is less or equal than other.
    #[allow(dead_code)]
    pub fn leq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Sign(d1), Self::Sign(d2)) => d1.leq(d2),
            (Self::Const(d1), Self::Const(d2)) => d1.leq(d2),
            (Self::Interval(d1), Self::Interval(d2)) => d1.leq(d2),
            _ => panic!("StateDomain::leq domain kind mismatch"),
        }
    }

    /// Compute an abstract state that corresponds to the assignment of
    /// the expression `expr` to the variable `var`, starting from the state `self`.
    pub fn assign(self, var: VarId, expr: &ArithExpr) -> Self {
        match self {
            Self::Sign(d) => d.assign(var, expr).cover(),
            Self::Const(d) => d.assign(var, expr).cover(),
            Self::Interval(d) => d.assign(var, expr).cover(),
        }
    }

    /// Filter the abstract state according to the boolean guard `b`.
    pub fn guard(self, b: &BoolExpr) -> Self {
        match self {
            Self::Sign(d) => d.guard(b).cover(),
            Self::Const(d) => d.guard(b).cover(),
            Self::Interval(d) => d.guard(b).cover(),
        }
    }
}

impl PartialEq for StateDomain {
    fn eq(&self, y: &Self) -> bool {
        match (self, y) {
            (Self::Sign(d1), Self::Sign(d2)) => d1 == d2,
            (Self::Const(d1), Self::Const(d2)) => d1 == d2,
            (Self::Interval(d1), Self::Interval(d2)) => d1 == d2,
            _ => panic!("StateDomain::eq domain kind mismatch"),
        }
    }
}

/// Kind of state domain.
#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
pub enum StateDomainKind {
    Sign,
    Const,
    Interval,
}

impl StateDomainKind {
    /// Returns the name of the domain of `self` kind.
    pub fn name(&self) -> String {
        match self {
            Self::Sign => "Sign".to_owned(),
            Self::Const => "Const".to_owned(),
            Self::Interval => "Interval".to_owned(),
        }
    }

    /// Returns the top element of the domain of `self` kind.
    pub fn top(&self) -> StateDomain {
        match self {
            Self::Sign => NonRela::top(IntDomainKind::Sign).cover(),
            Self::Const => NonRela::top(IntDomainKind::Const).cover(),
            Self::Interval => NonRela::top(IntDomainKind::Interval).cover(),
        }
    }

    /// Returns the bottom element of the domain of `self` kind.
    pub fn bottom(&self) -> StateDomain {
        match self {
            Self::Sign => NonRela::bottom(IntDomainKind::Sign).cover(),
            Self::Const => NonRela::bottom(IntDomainKind::Const).cover(),
            Self::Interval => NonRela::bottom(IntDomainKind::Interval).cover(),
        }
    }

    /// Return the best abstract element of kind `self` that for each variable
    /// in `intrv` contains the corresponding interval.
    pub fn from_interval(&self, intrv: &HashMap<VarId, (IntExt, IntExt)>) -> StateDomain {
        match self {
            Self::Sign => NonRela::from_interval(IntDomainKind::Sign, intrv).cover(),
            Self::Const => NonRela::from_interval(IntDomainKind::Const, intrv).cover(),
            Self::Interval => NonRela::from_interval(IntDomainKind::Interval, intrv).cover(),
        }
    }
}

/// Generic state domain induced by an `IntDomain`.
///
/// The state is implemented as a list of integer domains, one for each
/// variable of the program.
///
/// The domain functions implemented pointwise, e.g. the join of two `NonRela`
/// domains is computed by joining the int domains corresponding to the same
/// variable.
#[derive(Debug, Clone)]
pub struct NonRela {
    store: VarStore,
    kind: IntDomainKind,
}

impl PartialEq for NonRela {
    fn eq(&self, y: &Self) -> bool {
        assert_eq!(self.kind, y.kind);
        self.store == y.store
    }
}

impl NonRela {
    /// If a computation results in a bottom state for some variable, then the
    /// whole state domain must be bottom. This can be a problem because multiple
    /// representation of the bottom state are possible, making the comparison
    /// operators harder to implement.
    ///
    /// To solve this, `smash_bottom` should be called after each change of the
    /// domain to ensure that the domain is normalized, i.e. if any variable is
    /// bottom, then all the other variables are also set to bottom.
    fn smash_bottom(mut self) -> Self {
        let bottom = self.store.0.values().any(|d| *d == self.kind.bottom());

        if bottom {
            for d in self.store.0.values_mut() {
                *d = self.kind.bottom();
            }
        }

        self
    }

    /// Returns the top element of this domain.
    #[allow(dead_code)]
    fn top(kind: IntDomainKind) -> Self {
        Self::new_pointwise(|_| kind.top())
    }

    /// Returns the bottom element of this domain.
    fn bottom(kind: IntDomainKind) -> Self {
        Self::new_pointwise(|_| kind.bottom())
    }

    /// Return the best abstract element of kind `self` that for each variable
    /// in `intrv` contains the corresponding interval.
    fn from_interval(kind: IntDomainKind, intrv: &HashMap<VarId, (IntExt, IntExt)>) -> Self {
        Self::new_pointwise(|v| kind.from_interval(intrv[&v].0, intrv[&v].1))
    }

    /// Create a new `NonRela` by probing the generator function `f`
    /// with each variable.
    fn new_pointwise(mut f: impl FnMut(VarId) -> IntDomain) -> Self {
        let mut kind = None;

        let store = VarStore(
            VARS.lock()
                .unwrap()
                .iter()
                .map(|v| {
                    let d = f(*v);
                    match kind {
                        Some(kind) => assert_eq!(kind, d.kind()),
                        None => kind = Some(d.kind()),
                    };

                    (*v, f(*v))
                })
                .collect(),
        );

        NonRela {
            store,
            kind: kind.expect("VARS is empty"),
        }
    }

    /// Merge two `NonRela` by applying the function `f` for
    /// each variable and collecting the result in a new `NonRela`.
    fn binary_pointwise(
        self,
        mut other: Self,
        mut f: impl FnMut(IntDomain, IntDomain) -> IntDomain,
    ) -> Self {
        assert_eq!(self.kind, other.kind);
        let store = VarStore(
            self.store
                .0
                .into_iter()
                .map(|(v, x)| (v, f(x, other.store.0.remove(&v).expect("missing var"))))
                .collect(),
        );

        Self::smash_bottom(NonRela {
            store,
            kind: self.kind,
        })
    }
}

impl StateDomainTr for NonRela {
    fn join(self, other: Self) -> Self {
        self.binary_pointwise(other, |x, y| x.join(y))
    }

    fn meet(self, other: Self) -> Self {
        self.binary_pointwise(other, |x, y| x.meet(y))
    }

    fn widen(self, other: Self) -> Self {
        self.binary_pointwise(other, |x, y| x.widen(y))
    }

    fn narrow(self, other: Self) -> Self {
        self.binary_pointwise(other, |x, y| x.narrow(y))
    }

    fn leq(&self, other: &Self) -> bool {
        assert_eq!(self.kind, other.kind);
        self.store
            .0
            .iter()
            .map(|(v, x)| (x, &other.store[*v]))
            .all(|(x, y)| x <= y)
    }

    fn assign(mut self, var: VarId, expr: &ArithExpr) -> Self {
        let d = self.store.kind_eval(self.kind, expr);
        *self.store.0.get_mut(&var).expect("missing var") = d;
        Self::smash_bottom(self)
    }

    fn guard(self, b: &BoolExpr) -> Self {
        let kind = self.kind;

        // The filtering is implemented in an inductive fashion:
        //   * the base cases are `Const` and comparison operators: the former
        //   is trivially implemented, while the latter are implemented using
        //   the corresponding filtering functions of the integer domain.
        //   * the inductive cases are implemented using the composition and `join`
        //   respectively for the `and` and `or`. The `not` is implemented by
        //   calling the `guard` on the opposite boolean expression.
        let store = match b {
            BoolExpr::Const(true) => self.store,
            BoolExpr::Const(false) => VarStore::kind_bottom(self.kind),
            BoolExpr::And(b1, b2) => {
                let d1 = self.clone().guard(b1).guard(b2);
                let d2 = self.guard(b2).guard(b1);

                d1.meet(d2).store
            }
            BoolExpr::Or(b1, b2) => {
                let d1 = self.clone().guard(b1);
                let d2 = self.guard(b2);

                d1.join(d2).store
            }
            BoolExpr::Not(b) => match &**b {
                BoolExpr::Const(true) => VarStore::kind_bottom(self.kind),
                BoolExpr::Const(false) => self.store,
                BoolExpr::And(b1, b2) => self.guard(&(!*b1.clone() | !*b2.clone())).store,
                BoolExpr::Or(b1, b2) => self.guard(&(!*b1.clone() & !*b2.clone())).store,
                BoolExpr::Not(b) => self.guard(b).store,
                BoolExpr::Le(a1, a2) => self.guard(&BoolExpr::Ge(a1.clone(), a2.clone())).store,
                BoolExpr::Leq(a1, a2) => self.guard(&BoolExpr::Gt(a1.clone(), a2.clone())).store,
                BoolExpr::Gt(a1, a2) => self.guard(&BoolExpr::Leq(a1.clone(), a2.clone())).store,
                BoolExpr::Ge(a1, a2) => self.guard(&BoolExpr::Le(a1.clone(), a2.clone())).store,
                BoolExpr::Eq(a1, a2) => self.guard(&BoolExpr::Ne(a1.clone(), a2.clone())).store,
                BoolExpr::Ne(a1, a2) => self.guard(&BoolExpr::Eq(a1.clone(), a2.clone())).store,
            },
            BoolExpr::Eq(a1, a2) => self.kind.filter_eq(a1, a2, self.store),
            BoolExpr::Ne(a1, a2) => self.kind.filter_ne(a1, a2, self.store),
            BoolExpr::Le(a1, a2) => self.kind.filter_le(a1, a2, self.store),
            BoolExpr::Leq(a1, a2) => self.kind.filter_leq(a1, a2, self.store),
            BoolExpr::Gt(a1, a2) => self.kind.filter_gt(a1, a2, self.store),
            BoolExpr::Ge(a1, a2) => self.kind.filter_ge(a1, a2, self.store),
        };

        NonRela { store, kind }
    }

    fn cover(self) -> StateDomain {
        match self.kind {
            IntDomainKind::Sign => StateDomain::Sign(self),
            IntDomainKind::Const => StateDomain::Const(self),
            IntDomainKind::Interval => StateDomain::Interval(self),
        }
    }

    fn uncover(d: StateDomain) -> Self {
        match d {
            StateDomain::Sign(d) => d,
            StateDomain::Const(d) => d,
            StateDomain::Interval(d) => d,
        }
    }
}

impl Index<VarId> for NonRela {
    type Output = IntDomain;

    fn index(&self, v: VarId) -> &Self::Output {
        &self.store[v]
    }
}

/// A `VarStore` is a simple mapping from a variable to an integer
/// domain element.
///
/// This is used to implement `NonRela`.
#[derive(Debug, Clone, PartialEq)]
pub struct VarStore(HashMap<VarId, IntDomain>);

impl VarStore {
    /// Compute an abstract element corresponding to the evaluation of
    /// the arithmetic expression `expr`, assuming that the kind of the
    /// integer domain stored in this `VarStore` is of type `D`.
    ///
    /// If the type does not match, then the function may panic.
    pub fn eval<D: IntDomainTr>(&self, expr: &ArithExpr) -> D {
        match expr {
            ArithExpr::Interval(a, b) => D::from_interval(*a, *b),
            ArithExpr::Var(v) => D::uncover(self.0[v].clone()),
            ArithExpr::Sum(l, r) => self.eval::<D>(l) + self.eval::<D>(r),
            ArithExpr::Sub(l, r) => self.eval::<D>(l) - self.eval::<D>(r),
            ArithExpr::Mul(l, r) => self.eval::<D>(l) * self.eval::<D>(r),
            ArithExpr::Div(l, r) => self.eval::<D>(l) / self.eval::<D>(r),
        }
    }

    /// Compute an abstract element corresponding to the evaluation of
    /// the arithmetic expression `expr`, assuming that the kind of the
    /// integer domain stored in this `VarStore` is of kind `kind`.
    pub fn kind_eval(&self, kind: IntDomainKind, expr: &ArithExpr) -> IntDomain {
        match kind {
            IntDomainKind::Sign => self.eval::<SignDomain>(expr).cover(),
            IntDomainKind::Const => self.eval::<ConstDomain>(expr).cover(),
            IntDomainKind::Interval => self.eval::<IntervalDomain>(expr).cover(),
        }
    }

    /// Returns a `VarStore` in which every variable is mapped to a
    /// bottom element of a integer domain of type `D`.
    pub fn bottom<D: IntDomainTr>() -> VarStore {
        VarStore(
            VARS.lock()
                .unwrap()
                .iter()
                .map(|v| (*v, D::bottom().cover()))
                .collect(),
        )
    }

    /// Returns a `VarStore` in which every variable is mapped to a
    /// bottom element of a integer domain of kind `kind`.
    pub fn kind_bottom(kind: IntDomainKind) -> VarStore {
        match kind {
            IntDomainKind::Sign => Self::bottom::<SignDomain>(),
            IntDomainKind::Const => Self::bottom::<ConstDomain>(),
            IntDomainKind::Interval => Self::bottom::<IntervalDomain>(),
        }
    }
}

impl Index<VarId> for VarStore {
    type Output = IntDomain;

    fn index(&self, v: VarId) -> &Self::Output {
        &self.0[&v]
    }
}

impl IndexMut<VarId> for VarStore {
    fn index_mut(&mut self, v: VarId) -> &mut Self::Output {
        self.0
            .get_mut(&v)
            .expect("VarStore::IndexMut missing variable")
    }
}

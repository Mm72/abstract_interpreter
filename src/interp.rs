//! This module implements the abstract intepreter.
//!
//! The interpreter takes in input a program and proceeds with its
//! analysis. For the entry point label it sets all the abstract
//! domains to `top`, while for all the other program labels they are
//! initialized with `bottom`. Then the analyzer computes the abstract
//! elements of all the statements up to termination.
//!
//! The interpreter can be configured so that during the analysis, at
//! some specific points, it takes a snapshot of the abstract state.
//! Those snapshots are called *tracepoints*. They are created according
//! to the interpreter settings. The enum `TracePointKind` identifies a
//! kind of tracepoint and the settings store a list of kinds of tracepoints
//! that should be created during the analysis.
//!
//! At the end of the analysis the result is a list of tracepoints.

use std::collections::HashMap;
use std::default::Default;

use crate::ast::{ArithExpr, BoolExpr, IntExt, Stat, VarId};
use crate::domain::{StateDomain, StateDomainKind, DOMAINS};

/// List of all the kind of tracepoint available in this application.
pub const TRACEPOINTS: [TracePointKind; 8] = [
    TracePointKind::Assign,
    TracePointKind::Skip,
    TracePointKind::IfElse,
    TracePointKind::WhileIncoming,
    TracePointKind::WhileJoin,
    TracePointKind::WhileWiden,
    TracePointKind::WhileMeet,
    TracePointKind::WhileNarrow,
];

/// A `LabelStore` stores for each label a corresponding `DomainStore`.
pub type LabelStore = Vec<DomainStore>;

/// A `DomainStore` is a store which maps a kind of `StateDomain` to an
/// element of that kind.
#[derive(Debug, Clone, PartialEq)]
pub struct DomainStore(pub HashMap<StateDomainKind, StateDomain>);

impl DomainStore {
    /// Returns a store with `kinds` category of domains, all initialized
    /// with top.
    #[allow(dead_code)]
    fn top(kinds: &[StateDomainKind]) -> DomainStore {
        DomainStore(kinds.iter().map(|kind| (*kind, kind.top())).collect())
    }

    /// Returns a store with `kinds` category of domains, all initialized
    /// with bottom.
    fn bottom(kinds: &[StateDomainKind]) -> DomainStore {
        DomainStore(kinds.iter().map(|kind| (*kind, kind.bottom())).collect())
    }

    /// Return a store with `kinds` category of domains, such that each domain
    /// is initialized from `intrv`.
    fn from_interval(
        kinds: &[StateDomainKind],
        intrv: &HashMap<VarId, (IntExt, IntExt)>,
    ) -> DomainStore {
        DomainStore(
            kinds
                .iter()
                .map(|kind| (*kind, kind.from_interval(intrv)))
                .collect(),
        )
    }

    /// Compute the pointwise assignment of an expression `e` to a
    /// variable `v`.
    fn assign(&self, v: VarId, e: &ArithExpr) -> Self {
        DomainStore(
            self.0
                .iter()
                .map(|(kind, d)| (*kind, d.clone().assign(v, e)))
                .collect(),
        )
    }

    /// Compute the pointwise filtering with a guard `g`.
    fn filter(self, g: &BoolExpr) -> Self {
        DomainStore(
            self.0
                .into_iter()
                .map(|(kind, d)| (kind, d.guard(g)))
                .collect(),
        )
    }

    fn pointwise_binary(
        self,
        mut other: Self,
        mut f: impl FnMut(StateDomain, StateDomain) -> StateDomain,
    ) -> Self {
        DomainStore(
            self.0
                .into_iter()
                .map(|(kind, dom)| (kind, dom, other.0.remove(&kind).expect("missing domain")))
                .map(|(kind, dom, other_dom)| (kind, f(dom, other_dom)))
                .collect(),
        )
    }

    /// Returns the pointwise join between `self` and `other`.
    fn join(self, other: Self) -> Self {
        self.pointwise_binary(other, |x, y| x.join(y))
    }

    /// Returns the pointwise widening of `self` with `other`.
    fn widen(self, other: Self) -> Self {
        self.pointwise_binary(other, |x, y| x.widen(y))
    }

    /// Returns the pointwise meet between `self` and `other`.
    fn meet(self, other: Self) -> Self {
        self.pointwise_binary(other, |x, y| x.meet(y))
    }

    /// Returns the pointwise narrowing of `self` with `other`.
    fn narrow(self, other: Self) -> Self {
        self.pointwise_binary(other, |x, y| x.narrow(y))
    }
}

/// Category of tracepoint.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TracePointKind {
    /// A tracepoint is collected at an assignment statement.
    Assign,

    /// A tracepoint is collected at a skip statement.
    Skip,

    /// A tracepoint is collected at a if-then-else statement.
    IfElse,

    /// A tracepoint is collected before the entry of a while loop.
    WhileIncoming,

    /// A tracepoint is collected when a join operation occurs in
    /// a while loop header.
    WhileJoin,

    /// A tracepoint is collected when the loop header invariant is
    /// widened.
    WhileWiden,

    /// A tracepoint is collected when a meet operation occurs in
    /// a while loop header.
    WhileMeet,

    /// A tracepoint is collected when the loop header invariant is
    /// narrowed.
    WhileNarrow,
}

impl TracePointKind {
    /// Returns the name of a tracepoint kind.
    pub fn name(self) -> String {
        match self {
            Self::Assign => "Assign".to_owned(),
            Self::Skip => "Skip".to_owned(),
            Self::IfElse => "If-else".to_owned(),
            Self::WhileIncoming => "While incoming".to_owned(),
            Self::WhileJoin => "While join".to_owned(),
            Self::WhileWiden => "While widening".to_owned(),
            Self::WhileMeet => "While meet".to_owned(),
            Self::WhileNarrow => "While narrow".to_owned(),
        }
    }
}

/// A tracepoint contains a snapshot of the state of the interpreter
/// at a certain point of the analysis.
///
/// It store also the label which triggered its creation of its own kind.
#[derive(Debug, Clone)]
pub struct TracePoint {
    pub store: LabelStore,
    pub label: usize,
    pub kind: TracePointKind,
}

/// Settings of the intepreter.
#[derive(Debug, Clone)]
pub struct InterpSettings {
    /// Initialization state of the interpreter. It maps each variable
    /// to an interval (represented through its extreme values).
    pub init: HashMap<VarId, (IntExt, IntExt)>,

    /// Maximum number of join iterations to perform at the loop header
    /// before starting to apply the widening operator.
    pub join_iter: usize,

    /// Maximum number of meet iterations to perform at the loop header
    /// before starting to apply the narrowing operator.
    pub meet_iter: usize,

    /// If `Some`, it contains the maximum number narrowing iterations
    /// to perform at the loop header. Otherwise there is no limit.
    pub narrow_iter: Option<usize>,

    /// Kinds of tracepoints to collect.
    pub trace_kinds: Vec<TracePointKind>,

    /// Kinds of domains to consider for the analysis.
    pub domain_kinds: Vec<StateDomainKind>,
}

impl Default for InterpSettings {
    fn default() -> Self {
        InterpSettings {
            init: HashMap::new(),
            join_iter: 10,
            meet_iter: 10,
            narrow_iter: None,
            trace_kinds: TRACEPOINTS.to_vec(),
            domain_kinds: DOMAINS.to_vec(),
        }
    }
}

#[derive(PartialEq, Eq)]
enum WhileHelperMode {
    Join,
    Widen,
    Meet,
    Narrow,
    Done,
}

/// Internal helper struct used to track the status of the interpreter
/// at a while loop analysis.
struct WhileHelper<'a> {
    mode: WhileHelperMode,
    cnt: usize,
    settings: &'a InterpSettings,
}

impl<'a> WhileHelper<'a> {
    fn new(settings: &'a InterpSettings) -> Self {
        let mode = if settings.join_iter == 0 {
            WhileHelperMode::Widen
        } else {
            WhileHelperMode::Join
        };

        WhileHelper {
            mode,
            cnt: 0,
            settings,
        }
    }

    fn increment(&mut self) {
        self.cnt += 1;

        match self.mode {
            WhileHelperMode::Join => {
                if self.cnt >= self.settings.join_iter {
                    self.mode = WhileHelperMode::Widen;
                    self.cnt = 0;
                }
            }
            WhileHelperMode::Widen => (),
            WhileHelperMode::Meet => {
                if self.cnt > self.settings.meet_iter {
                    self.mode = WhileHelperMode::Narrow;
                    self.cnt = 0;
                }
            }
            WhileHelperMode::Narrow => {
                if let Some(narrow_iter) = self.settings.narrow_iter {
                    if self.cnt >= narrow_iter {
                        self.mode = WhileHelperMode::Done;
                    }
                }
            }
            WhileHelperMode::Done => (),
        }
    }

    fn set_stable(&mut self, stable: bool) {
        if stable {
            match self.mode {
                WhileHelperMode::Widen => {
                    self.mode = if self.settings.meet_iter > 0 {
                        WhileHelperMode::Meet
                    } else {
                        match self.settings.narrow_iter {
                            Some(0) => WhileHelperMode::Done,
                            _ => WhileHelperMode::Narrow,
                        }
                    };

                    self.cnt = 0;
                }
                WhileHelperMode::Join
                | WhileHelperMode::Meet
                | WhileHelperMode::Narrow
                | WhileHelperMode::Done => {
                    self.mode = WhileHelperMode::Done;
                }
            }
        }
    }

    fn should_widen(&self) -> bool {
        matches!(self.mode, WhileHelperMode::Widen)
    }

    fn should_meet(&self) -> bool {
        matches!(self.mode, WhileHelperMode::Meet | WhileHelperMode::Narrow)
    }

    fn should_narrow(&self) -> bool {
        matches!(self.mode, WhileHelperMode::Narrow)
    }

    fn done(&self) -> bool {
        self.mode == WhileHelperMode::Done
    }
}

/// Internal state of the abstract interpreter.
#[derive(Debug, Clone)]
struct Interpreter<'a> {
    settings: &'a InterpSettings,
    tracepoints: Vec<TracePoint>,
    store: LabelStore,
}

impl<'a> Interpreter<'a> {
    /// Construct a new interpreter starting from a configuration and the
    /// number of labels in the program to analyze.
    fn new(settings: &'a InterpSettings, stat_len: usize) -> Interpreter {
        Interpreter {
            settings,
            tracepoints: Vec::new(),
            store: (0..stat_len)
                .into_iter()
                .map(|i| {
                    if i > 0 {
                        DomainStore::bottom(&settings.domain_kinds)
                    } else {
                        DomainStore::from_interval(&settings.domain_kinds, &settings.init)
                    }
                })
                .collect(),
        }
    }

    /// Analyze a statement, returning the abstract element corresponding
    /// to its postcondition.
    fn analyze(&mut self, stat: &Stat) -> DomainStore {
        match &stat {
            Stat::Assign(v, e, label) => {
                self.trace(TracePointKind::Assign, *label, None);
                self.store[*label].assign(*v, e)
            }
            Stat::Skip(label) => {
                self.trace(TracePointKind::Skip, *label, None);
                self.store[*label].clone()
            }
            Stat::Concat(s1, s2) => {
                self.store[s2.get_first_label()] = self.analyze(s1);
                self.analyze(s2)
            }
            Stat::IfElse(g, t, e, label) => {
                self.trace(TracePointKind::IfElse, *label, None);
                self.store[t.get_first_label()] = self.store[*label].clone().filter(g);
                self.store[e.get_first_label()] = self.store[*label].clone().filter(&!g.clone());

                let then_dom = self.analyze(t);
                let else_dom = self.analyze(e);

                then_dom.join(else_dom)
            }
            Stat::While(g, t, bef, head_label) => {
                self.trace(TracePointKind::WhileIncoming, *bef, None);
                let mut head = self.store[*bef].clone();
                let mut helper = WhileHelper::new(self.settings);

                self.store[*head_label] = head.clone();

                loop {
                    let prev_head = self.store[*head_label].clone();

                    // Run the body of the loop.
                    self.store[t.get_first_label()] = self.store[*head_label].clone().filter(g);
                    let body = self.analyze(t);

                    head = body.join(self.store[*bef].clone());
                    self.trace(TracePointKind::WhileJoin, *head_label, Some(head.clone()));
                    helper.set_stable(head == prev_head);

                    if helper.should_widen() {
                        head = prev_head.clone().widen(head);
                        self.trace(TracePointKind::WhileWiden, *head_label, Some(head.clone()));
                        helper.set_stable(head == prev_head);

                        if helper.done() {
                            break;
                        } else {
                            self.store[*head_label] = head;
                            helper.increment();
                            continue;
                        }
                    }

                    if helper.should_meet() {
                        head = prev_head.clone().meet(head);
                        self.trace(TracePointKind::WhileMeet, *head_label, Some(head.clone()));
                        helper.set_stable(head == prev_head);
                    }

                    if helper.should_narrow() {
                        head = prev_head.clone().narrow(head);
                        self.trace(TracePointKind::WhileNarrow, *head_label, Some(head.clone()));
                        helper.set_stable(head == prev_head);
                    }

                    self.store[*head_label] = head;

                    if helper.done() {
                        break;
                    }

                    helper.increment();
                }

                self.store[*head_label].clone().filter(&!g.clone())
            }
        }
    }

    /// Helper function called when a possible tracepoint is reached. If
    /// the kind is enabled then a tracepoint is collected. Optionally, the
    /// store to save can be provided explicitly.
    fn trace(&mut self, kind: TracePointKind, label: usize, store: Option<DomainStore>) {
        let store = store
            .map(|swap_dom| {
                let mut store = self.store.clone();
                store[label] = swap_dom;
                store
            })
            .unwrap_or_else(|| self.store.clone());

        if self.settings.trace_kinds.contains(&kind) {
            self.tracepoints.push(TracePoint { store, kind, label });
        }
    }
}

/// User facing function for the analysis of a program.
///
/// The required parameters are a statement (program) `stat` and the
/// `settings` of the interpreter. In output a list of tracepoints is
/// provided.
pub fn analyze(stat: &Stat, settings: &InterpSettings) -> Vec<TracePoint> {
    let mut interp = Interpreter::new(settings, stat.len());
    interp.analyze(stat);
    interp.tracepoints
}

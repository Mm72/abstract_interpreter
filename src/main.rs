//! Introduction
//! ============
//!
//! This application denoted as **abstract_interpreter** implements a very basic
//! abstract intepreter, i.e. a static analysis tool that analyzes an input
//! program to deduce some *abstract invariants* over it.
//!
//! Features
//! ========
//!
//! * The input is a program written in a toy language that supports just the basic
//! commands such as assignments, conditionals and loops. For more details check out
//! the `parser` module.
//! * The only supported data type is signed integer. For the purpose of this tool
//! they are encoded as `i32` but the analysis works as if they were unlimited precision
//! integers (e.g. the output of the analysis can be `+inf` or `-inf`).
//! * The following abstract domains are supported:
//!     - **Sign:** this domain tracks the sign of a variable.
//!     - **Const:** this domain tracks the values of constants variables.
//!     - **Interval:** this domain tracks an interval in which a variable must lie in.
//! * The output of the analysis is browsable through a Terminal User Interface (TUI)
//! implemented using the [`tui-rs`](https://github.com/fdehau/tui-rs) library.
//!
//! Usage example
//! =============
//!
//! Let's consider a very basic program:
//!
//! ```text
//! // Initialize a variable with a constant.
//! // Notice that C/C++ style comments are supported.
//! // Also variables are denoted with a `v` followed by an index.
//! v1 := 0
//!
//! // This variable is initialized with a range. This means
//! // that the analyzer will assume that for any (concrete)
//! // execution of the program the value of the variable will be
//! // in such range.
//! v2 := [0, 10]
//!
//! if v1 == v2
//!         // Here the range of v2 collapses to just 0
//!         // because the guard of the `if` filters all the
//!         // states in which v2 is not 0.
//!
//!         // This can be made evident by dropping a skip
//!         // statements (which acts like a nop) and checking
//!         // in the UI the invariant corresponding to the
//!         // label of the skip.
//!         skip
//!
//! else
//!         // The same can be done on the else branch.
//!         skip
//!
//! // Finally, the analyzer supports also loops!
//! // There are two interesting invariants to collect about loops:
//! //   * The precondition, i.e. what is true before entering the loop.
//! //   * The header, i.e. what is true before the loop guard but after joining
//! //   with the previous iteration.
//! // Those are denoted with two distinct labels (the former before the
//! // `while` keyword and the latter after that).
//! // The invariant (for v1) at the precondition is [0,0] (=0).
//! // The invariant at the loop header is [0,5].
//! while v1 < 5
//!         // Here the invariant is [0,4]
//!         v1 := v1 + 1
//!         skip
//!
//! // The invariant for this skip is obtained by filtering the loop header
//! // with the opposite of the guard (v1 >= 5), hence it is [5,5] (=5),
//! // as expected.
//! skip
//! ```
//!
//! Save it in `examples/simple` and the run the app by typing `cargo run`.
//! By typing `F1` you can access the pile picker and from that select the `simple`
//! program you've just saved. After that the main view will reappear with
//! the program fully analyzed.
//! Use the `Up`,`Down` keys to move around the program analysis and the `Left`,
//! `Right` keys to select a different abstact domain.

use std::env;
use std::error::Error;
use std::io;
use std::os::unix::fs::OpenOptionsExt;
use std::sync::Mutex;

#[macro_use]
extern crate lazy_static;

extern crate pest;
#[macro_use]
extern crate pest_derive;

use std::fs::OpenOptions;
use std::path::Path;

use simplelog::*;
use termion::raw::IntoRawMode;
use tui::backend::TermionBackend;
use tui::Terminal;

mod ast;
mod domain;
mod error;
mod interp;
mod parser;
mod ui;

use crate::ast::VarId;
use ui::events_loop;

/// Name of the directory in which the FIFO log file should
/// be placed.
const LOG_FIFO_ENV_VAR: &str = "XDG_RUNTIME_DIR";

/// Base name of the FIFO log file.
const LOG_FIFO_NAME: &str = "abstract_interpreter";

lazy_static! {
    /// List of all the variables seen in the current program.
    /// Empty until a program is loaded.
    static ref VARS: Mutex<Vec<VarId>> = Mutex::new(vec![]);
}

/// Initialize the logger.
///
/// # Returns
///
/// A `Result` which may contain some error if the FIFO file
/// failed to open or if the logger failed to load.
fn init_log() -> Result<(), Box<dyn Error>> {
    if cfg!(unix) {
        let log_fifo_path =
            env::var(LOG_FIFO_ENV_VAR).map(|dir| Path::new(&dir).join(LOG_FIFO_NAME))?;

        let log_fifo = OpenOptions::new()
            .write(true)
            .custom_flags(libc::O_NONBLOCK)
            .open(log_fifo_path)?;

        WriteLogger::init(LevelFilter::Info, Config::default(), log_fifo)?;
    }

    Ok(())
}

/// Program entry point.
#[allow(unused_must_use)]
fn main() -> Result<(), Box<dyn Error>> {
    // Initialize the logger.
    init_log();

    // Initialize the terminal for TUI usage.
    let stdout = io::stdout().into_raw_mode()?;
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // Run the events loop. This function continues
    // until user closes the program (by typing Ctrl+C).
    events_loop(&mut terminal)?;

    Ok(())
}

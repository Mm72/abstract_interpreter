//! This module implements the data structures used for the program
//! representation.

use std::cmp::{Ordering, PartialOrd};
use std::convert::From;
use std::fmt;
use std::ops::{Add, BitAnd, BitOr, Div, Mul, Neg, Not, Sub};
use std::str::FromStr;

use crate::error::ParserErr;

/// Integer type for variable's index.
pub type VarId = u32;

/// Integer data type of program's numerical values.
pub type IntTp = i32;

/// Plus infinite.
pub const PINF: IntExt = IntExt::PlusInf;

/// Minus infinite.
pub const MINF: IntExt = IntExt::MinusInf;

/// `IntExt` represents an "extended" integer value, i.e. regular
/// numbers plus +- infinite.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum IntExt {
    PlusInf,
    Int(IntTp),
    MinusInf,
}

/// Data structure which holds an arithmetic expression.
///
/// It is defined inductively. The base cases are intervals and
/// variables, while the inductive ones are the binary arithmetic
/// operators of sum, subtraction, multiplication and division.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ArithExpr {
    Interval(IntExt, IntExt),
    Var(VarId),
    Sum(Box<ArithExpr>, Box<ArithExpr>),
    Sub(Box<ArithExpr>, Box<ArithExpr>),
    Mul(Box<ArithExpr>, Box<ArithExpr>),
    Div(Box<ArithExpr>, Box<ArithExpr>),
}

impl ArithExpr {
    /// Returns the list of variables referenced in this expression.
    pub fn free_vars(&self) -> Vec<VarId> {
        fn rec(expr: &ArithExpr, vec: &mut Vec<VarId>) {
            match expr {
                ArithExpr::Interval(_, _) => (),
                ArithExpr::Var(v) => {
                    vec.push(*v);
                }
                ArithExpr::Sum(l, r) => {
                    rec(l, vec);
                    rec(r, vec);
                }
                ArithExpr::Sub(l, r) => {
                    rec(l, vec);
                    rec(r, vec);
                }
                ArithExpr::Mul(l, r) => {
                    rec(l, vec);
                    rec(r, vec);
                }
                ArithExpr::Div(l, r) => {
                    rec(l, vec);
                    rec(r, vec);
                }
            }
        }

        let mut vec = Vec::new();

        rec(self, &mut vec);
        vec.sort_unstable();
        vec.dedup();

        vec
    }
}

/// Data structure which holds an boolean expression.
///
/// It is defined inductively. The base cases are constants and
/// comparison operators (equality, non-equality, less, less-equal,
/// greater,greater-equal), while the inductive ones are the binary
/// logic operators AND,OR,NOT.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum BoolExpr {
    Const(bool),
    Eq(ArithExpr, ArithExpr),
    Ne(ArithExpr, ArithExpr),
    Le(ArithExpr, ArithExpr),
    Leq(ArithExpr, ArithExpr),
    Gt(ArithExpr, ArithExpr),
    Ge(ArithExpr, ArithExpr),
    Not(Box<BoolExpr>),
    And(Box<BoolExpr>, Box<BoolExpr>),
    Or(Box<BoolExpr>, Box<BoolExpr>),
}

impl BoolExpr {
    /// Returns the list of variables referenced in this expression.
    pub fn free_vars(&self) -> Vec<VarId> {
        fn rec(expr: &BoolExpr, vec: &mut Vec<VarId>) {
            match expr {
                BoolExpr::Const(_) => (),
                BoolExpr::Eq(l, r) => {
                    vec.append(&mut l.free_vars());
                    vec.append(&mut r.free_vars())
                }
                BoolExpr::Ne(l, r) => {
                    vec.append(&mut l.free_vars());
                    vec.append(&mut r.free_vars())
                }
                BoolExpr::Le(l, r) => {
                    vec.append(&mut l.free_vars());
                    vec.append(&mut r.free_vars())
                }
                BoolExpr::Leq(l, r) => {
                    vec.append(&mut l.free_vars());
                    vec.append(&mut r.free_vars())
                }
                BoolExpr::Gt(l, r) => {
                    vec.append(&mut l.free_vars());
                    vec.append(&mut r.free_vars())
                }
                BoolExpr::Ge(l, r) => {
                    vec.append(&mut l.free_vars());
                    vec.append(&mut r.free_vars())
                }
                BoolExpr::Not(l) => {
                    vec.append(&mut l.free_vars());
                }
                BoolExpr::And(l, r) => {
                    vec.append(&mut l.free_vars());
                    vec.append(&mut r.free_vars())
                }
                BoolExpr::Or(l, r) => {
                    vec.append(&mut l.free_vars());
                    vec.append(&mut r.free_vars())
                }
            };
        }

        let mut vec = Vec::new();

        rec(self, &mut vec);
        vec.sort_unstable();
        vec.dedup();

        vec
    }
}

/// A Statement is a data structure which encodes programs.
///
/// It is defined inductively. The base cases are assignments and
/// skip, while the inductive ones are concatenation of statments,
/// if-the-else and while loops.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Stat {
    Assign(VarId, ArithExpr, usize),
    Skip(usize),
    Concat(Box<Stat>, Box<Stat>),
    IfElse(BoolExpr, Box<Stat>, Box<Stat>, usize),
    While(BoolExpr, Box<Stat>, usize, usize),
}

impl Stat {
    /// Compute recursively the number of labels in this statement.
    pub fn len(&self) -> usize {
        match &self {
            Stat::Assign(_, _, _) => 1,
            Stat::Skip(_) => 1,
            Stat::Concat(s1, s2) => s1.len() + s2.len(),
            Stat::IfElse(_, t, e, _) => t.len() + e.len() + 1,
            Stat::While(_, t, _, _) => t.len() + 2,
        }
    }

    /// Returns the list of variables referenced in this statement.
    pub fn free_vars(&self) -> Vec<VarId> {
        fn rec(stat: &Stat, vec: &mut Vec<VarId>) {
            match stat {
                Stat::Assign(v, e, _) => {
                    vec.append(&mut e.free_vars());
                    vec.push(*v)
                }
                Stat::Skip(_) => (),
                Stat::Concat(s1, s2) => {
                    vec.append(&mut s1.free_vars());
                    vec.append(&mut s2.free_vars());
                }
                Stat::IfElse(g, t, e, _) => {
                    vec.append(&mut g.free_vars());
                    vec.append(&mut t.free_vars());
                    vec.append(&mut e.free_vars());
                }
                Stat::While(g, t, _, _) => {
                    vec.append(&mut g.free_vars());
                    vec.append(&mut t.free_vars());
                }
            }
        }

        let mut vec = Vec::new();

        rec(self, &mut vec);
        vec.sort_unstable();
        vec.dedup();

        vec
    }

    /// Set recursively the labels to this statement and to
    /// its children. The value for the first label is `0`.
    pub fn set_labels(&mut self) {
        let mut cnt = 0;
        self.set_labels_rec(&mut cnt);
    }

    fn set_labels_rec(&mut self, cur: &mut usize) {
        match self {
            Stat::Assign(_, _, label) => {
                *label = *cur;
                *cur += 1;
            }
            Stat::Skip(label) => {
                *label = *cur;
                *cur += 1;
            }
            Stat::Concat(s1, s2) => {
                s1.set_labels_rec(cur);
                s2.set_labels_rec(cur);
            }
            Stat::IfElse(_, t, e, label) => {
                *label = *cur;
                *cur += 1;
                t.set_labels_rec(cur);
                e.set_labels_rec(cur);
            }
            Stat::While(_, t, bef, head) => {
                *bef = *cur;
                *cur += 1;
                *head = *cur;
                *cur += 1;
                t.set_labels_rec(cur);
            }
        }
    }

    /// If found, return a reference to a statement with label equal to
    /// `label`.
    pub fn get_with_label(&self, label: usize) -> Option<&Stat> {
        match self {
            Stat::Assign(_, _, this) => {
                if label == *this {
                    Some(self)
                } else {
                    None
                }
            }
            Stat::Skip(this) => {
                if label == *this {
                    Some(self)
                } else {
                    None
                }
            }
            Stat::Concat(s1, s2) => s1
                .get_with_label(label)
                .or_else(|| s2.get_with_label(label)),
            Stat::IfElse(_, t, e, this) => {
                if label == *this {
                    Some(self)
                } else {
                    t.get_with_label(label).or_else(|| e.get_with_label(label))
                }
            }
            Stat::While(_, t, bef, head) => {
                if label == *bef || label == *head {
                    Some(self)
                } else {
                    t.get_with_label(label)
                }
            }
        }
    }

    /// Returns the first label reached by the control when executing this
    /// statement.
    pub fn get_first_label(&self) -> usize {
        match self {
            Stat::Assign(_, _, label) => *label,
            Stat::Skip(label) => *label,
            Stat::Concat(s, _) => s.get_first_label(),
            Stat::IfElse(_, _, _, label) => *label,
            Stat::While(_, _, bef, _) => *bef,
        }
    }
}

impl fmt::Display for IntExt {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Int(i) => write!(f, "{}", i),
            Self::PlusInf => write!(f, "+inf"),
            Self::MinusInf => write!(f, "-inf"),
        }
    }
}

impl FromStr for IntExt {
    type Err = ParserErr;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s == "+inf" {
            Ok(PINF)
        } else if s == "-inf" {
            Ok(MINF)
        } else {
            i32::from_str(s)
                .map(Self::Int)
                .map_err(|e| ParserErr::InvalidConst(s.to_owned(), e))
        }
    }
}

impl From<i32> for IntExt {
    fn from(i: i32) -> Self {
        IntExt::Int(i)
    }
}

impl fmt::Display for ArithExpr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Interval(a, b) => {
                if a == b {
                    write!(f, "{}", a)
                } else {
                    write!(f, "[{}, {}]", a, b)
                }
            }
            Self::Var(v) => write!(f, "v{}", v),
            Self::Sum(l, r) => write!(f, "{} + {}", l, r),
            Self::Sub(l, r) => write!(f, "{} - {}", l, r),
            Self::Mul(l, r) => write!(f, "({} * {})", l, r),
            Self::Div(l, r) => write!(f, "({} / {})", l, r),
        }
    }
}

impl fmt::Display for BoolExpr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Const(b) => write!(f, "{}", b),
            Self::Eq(l, r) => write!(f, "{} == {}", l, r),
            Self::Ne(l, r) => write!(f, "{} != {}", l, r),
            Self::Le(l, r) => write!(f, "{} < {}", l, r),
            Self::Leq(l, r) => write!(f, "{} <= {}", l, r),
            Self::Gt(l, r) => write!(f, "{} > {}", l, r),
            Self::Ge(l, r) => write!(f, "{} >= {}", l, r),
            Self::Not(b) => write!(f, "!({})", b),
            Self::And(l, r) => write!(f, "({}) && ({})", l, r),
            Self::Or(l, r) => write!(f, "({}) || ({})", l, r),
        }
    }
}

impl fmt::Display for Stat {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Stat::Assign(v, a, label) => write!(f, "l{} v{} := {}", label, v, a),
            Stat::Skip(label) => write!(f, "l{} skip", label),
            Stat::Concat(l, r) => write!(f, "{}\n{}", l, r),
            Stat::IfElse(g, t, e, label) => {
                // Add an identation block.
                let mut t = format!("{}", t);
                t.insert(0, '\t');
                t = t.replace("\n", "\n\t");

                let mut e = format!("{}", e);
                e.insert(0, '\t');
                e = e.replace("\n", "\n\t");

                write!(f, "l{} if {}\n{}\nelse\n{}", label, g, t, e)
            }
            Stat::While(g, t, bef, head) => {
                let mut t = format!("{}", t);
                t.insert(0, '\t');
                t = t.replace("\n", "\n\t");

                write!(f, "l{} while l{} {}\n{}", bef, head, g, t)
            }
        }
    }
}

impl Ord for IntExt {
    fn cmp(&self, other: &Self) -> Ordering {
        match (self, other) {
            (Self::PlusInf, Self::PlusInf) => Ordering::Equal,
            (Self::PlusInf, _) => Ordering::Greater,
            (Self::Int(_), Self::PlusInf) => Ordering::Less,
            (Self::Int(x), Self::Int(y)) => x.cmp(y),
            (Self::Int(_), Self::MinusInf) => Ordering::Greater,
            (Self::MinusInf, Self::MinusInf) => Ordering::Equal,
            (Self::MinusInf, _) => Ordering::Less,
        }
    }
}

impl PartialOrd for IntExt {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Add for IntExt {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        match (self, other) {
            (Self::PlusInf, Self::PlusInf) => PINF,
            (Self::PlusInf, Self::Int(_)) => PINF,
            (Self::Int(_), Self::PlusInf) => PINF,
            (Self::MinusInf, Self::MinusInf) => MINF,
            (Self::MinusInf, Self::Int(_)) => MINF,
            (Self::Int(_), Self::MinusInf) => MINF,
            (Self::Int(a), Self::Int(b)) => Self::Int(a + b),
            _ => panic!("IntExt::add sum between PINF and MINF is undef"),
        }
    }
}

impl Neg for IntExt {
    type Output = Self;

    fn neg(self) -> Self {
        match self {
            Self::PlusInf => MINF,
            Self::Int(i) => Self::Int(-i),
            Self::MinusInf => PINF,
        }
    }
}

impl Sub for IntExt {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        self + (-other)
    }
}

impl Mul for IntExt {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        match (self, other) {
            (Self::PlusInf, Self::PlusInf) => PINF,
            (Self::PlusInf, Self::Int(0)) => Self::Int(0),
            (Self::PlusInf, Self::Int(1..=IntTp::MAX)) => PINF,
            (Self::PlusInf, Self::Int(IntTp::MIN..=-1)) => MINF,
            (Self::PlusInf, Self::MinusInf) => MINF,
            (Self::MinusInf, Self::PlusInf) => MINF,
            (Self::MinusInf, Self::Int(0)) => Self::Int(0),
            (Self::MinusInf, Self::Int(1..=IntTp::MAX)) => MINF,
            (Self::MinusInf, Self::Int(IntTp::MIN..=-1)) => PINF,
            (Self::MinusInf, Self::MinusInf) => PINF,
            (Self::Int(0), _) => Self::Int(0),
            (Self::Int(1..=IntTp::MAX), Self::PlusInf) => PINF,
            (Self::Int(IntTp::MIN..=-1), Self::PlusInf) => MINF,
            (Self::Int(1..=IntTp::MAX), Self::MinusInf) => MINF,
            (Self::Int(IntTp::MIN..=-1), Self::MinusInf) => PINF,
            (Self::Int(a), Self::Int(b)) => Self::Int(a * b),
        }
    }
}

impl Div for IntExt {
    type Output = Self;

    fn div(self, other: Self) -> Self {
        match (self, other) {
            (Self::PlusInf, Self::PlusInf) => PINF,
            (Self::PlusInf, Self::Int(1..=IntTp::MAX)) => PINF,
            (Self::PlusInf, Self::Int(IntTp::MIN..=-1)) => MINF,
            (Self::PlusInf, Self::MinusInf) => MINF,
            (Self::MinusInf, Self::PlusInf) => MINF,
            (Self::MinusInf, Self::Int(1..=IntTp::MAX)) => MINF,
            (Self::MinusInf, Self::Int(IntTp::MIN..=-1)) => PINF,
            (Self::MinusInf, Self::MinusInf) => PINF,
            (Self::Int(0), Self::Int(IntTp::MIN..=-1)) => Self::Int(0),
            (Self::Int(0), Self::Int(1..=IntTp::MAX)) => Self::Int(0),
            (Self::Int(_), Self::PlusInf) => Self::Int(0),
            (Self::Int(_), Self::MinusInf) => Self::Int(0),
            (Self::Int(a), Self::Int(b)) => Self::Int(a / b),
            _ => panic!("IntExt::div division by zero"),
        }
    }
}

impl Add for ArithExpr {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        ArithExpr::Sum(Box::new(self), Box::new(other))
    }
}

impl Sub for ArithExpr {
    type Output = Self;

    fn sub(self, other: Self) -> Self::Output {
        ArithExpr::Sub(Box::new(self), Box::new(other))
    }
}

impl Mul for ArithExpr {
    type Output = Self;

    fn mul(self, other: Self) -> Self::Output {
        ArithExpr::Mul(Box::new(self), Box::new(other))
    }
}

impl Div for ArithExpr {
    type Output = Self;

    fn div(self, other: Self) -> Self::Output {
        ArithExpr::Div(Box::new(self), Box::new(other))
    }
}

impl BitAnd for BoolExpr {
    type Output = Self;

    fn bitand(self, other: Self) -> Self::Output {
        BoolExpr::And(Box::new(self), Box::new(other))
    }
}

impl Not for BoolExpr {
    type Output = Self;

    fn not(self) -> Self::Output {
        BoolExpr::Not(Box::new(self))
    }
}

impl BitOr for BoolExpr {
    type Output = Self;

    fn bitor(self, other: Self) -> Self::Output {
        BoolExpr::Or(Box::new(self), Box::new(other))
    }
}

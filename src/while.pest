arith_bound = @{ "-"? ~ ASCII_DIGIT+ | "+inf" | "-inf" }
arith_int = @{ ASCII_DIGIT+ }
arith_interval = { "[" ~ arith_bound  ~ "," ~ arith_bound ~ "]" }
arith_var_id = @{ ASCII_DIGIT+ }
arith_var = ${ "v" ~ arith_var_id }

arith_add = { "+" }
arith_sub = { "-" }
arith_mul = { "*" }
arith_div = { "/" }
arith_op = _{ arith_add | arith_sub | arith_mul | arith_div }

// Note: the `"" ~` is needed to correctly parse whitespaces at the
// beginning of the expression. For example: `  10`.
arith_neg = ${ "-" ~ arith_term }
arith_term = _{ "(" ~ arith_expr ~ ")" | arith_interval | arith_int | arith_var | arith_neg }
arith_expr = { "" ~ arith_term ~ (arith_op ~ arith_term)* }

bool_true = { "true" }
bool_false = { "false" }
bool_const = _{ bool_true | bool_false }

bool_eq = { "==" }
bool_neq = { "!=" }
bool_le = { "<" }
bool_leq = { "<=" }
bool_gt = { ">" }
bool_ge = { ">=" }
bool_arith_rela_op = _{ bool_eq | bool_neq | bool_leq | bool_le | bool_ge | bool_gt }
bool_arith_term = { arith_expr ~ bool_arith_rela_op ~ arith_expr }

bool_and = { "&&" }
bool_not = _{ "!" }
bool_or  = { "||" }

bool_expr = { "" ~ bool_lit ~ ((bool_and | bool_or) ~ bool_lit)* }
bool_term = _{ bool_arith_term | bool_const | bool_inv_term | "(" ~ bool_expr ~ ")" }
bool_inv_term = { bool_not ~ bool_term }
bool_lit = _{ bool_term | bool_inv_term }

stat_ident = _{ "\t"* }
stat_ident_next = _{ PEEK ~ "\t" }
stat_ident_peek = _{ PEEK }
stat_ident_push = _{ PUSH(stat_ident) }
stat_ident_pop = _{ POP }

stat_empty = _{ "" }
stat_skip = { "skip" }
stat_assign = { arith_var ~ ":=" ~ arith_expr }
stat_base = _{ ((stat_skip | stat_assign | stat_empty) ~ "\n"+) | stat_if_else | stat_while }
stat_base_check = _{ ((stat_skip | stat_assign | stat_empty) ~ "\n"+) | stat_if_else_check | stat_while }
stat_base_peek = _{ stat_ident_peek ~ stat_base }
stat_base_push = _{ stat_ident_push ~ stat_base }
stat_base_pop = _{ stat_ident_pop ~ stat_base }

stat_block_single_check = { &stat_ident_next ~ PUSH(stat_ident) ~ stat_base_check ~ !stat_ident_peek }
stat_block_multiple_check = { &stat_ident_next ~ PUSH(stat_ident) ~ stat_base_check ~ stat_base_peek }
stat_block_single = _{ stat_ident ~ stat_base }
stat_block_multiple = _{ stat_ident_push ~ stat_base ~ (&(stat_base_peek ~ stat_base_peek) ~ stat_base_peek)* ~ stat_base_pop }
stat_block = { (&stat_block_single_check ~ stat_block_single) | (&stat_block_multiple_check ~ stat_block_multiple) }

stat_block_noident = _{ stat_base_peek+ }

stat_if_else_check = { "if" ~ bool_expr ~ "\n"+ ~ stat_block ~ stat_ident_peek ~ "else" ~ "\n"+ ~ stat_block  }
stat_if_else = { "if" ~ bool_expr ~ "\n"+ ~ stat_block ~ stat_ident ~ "else" ~ "\n"+ ~ stat_block }
stat_while = { "while" ~ bool_expr ~ "\n"+ ~ stat_block }

program = _{ SOI ~ PUSH("") ~ stat_block_noident ~ EOI }

init_int = @{ "-"? ~ ASCII_DIGIT+ }
init_top = { "top" }
init_value = _{ SOI ~ (arith_interval | init_int | init_top) ~ EOI }

comment_c_style = _{ "/*" ~ (!"*/" ~ ANY)* ~ "*/" }
comment_cpp_style = _{ "//" ~ (!"\n" ~ ANY)* }

WHITESPACE = _{ " " }
COMMENT = _{ comment_c_style | comment_cpp_style }

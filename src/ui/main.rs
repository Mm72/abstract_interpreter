use std::error::Error;
use std::io;
use std::iter;

use termion::event::Key;
use termion::input::TermRead;
use tui::backend::Backend;
use tui::layout::{Constraint, Direction, Layout};
use tui::style::{Color, Modifier, Style};
use tui::symbols::DOT;
use tui::terminal::Terminal;
use tui::text::{Span, Spans};
use tui::widgets::{Block, Borders, Cell, Gauge, Paragraph, Row, Table, Tabs};
use tui::Frame;

use crate::ast::Stat;
use crate::domain::{NonRela, StateDomainKind, StateDomainTr};
use crate::interp::{analyze, LabelStore, TracePoint, TracePointKind};
use crate::ui::{standard_block, Settings, UiView, UiViewKind};
use crate::VARS;

const TAB_STR: &str = "    ";

pub struct MainView {
    trace: Vec<TracePoint>,
    trace_idx: usize,
    domain_idx: usize,
}

impl MainView {
    pub fn from_settings(settings: &Settings) -> Result<Self, Box<dyn Error>> {
        let trace = if let Some(stat) = &settings.stat {
            analyze(stat, &settings.interp)
        } else {
            Vec::new()
        };

        Ok(Self {
            trace,
            trace_idx: 0,
            domain_idx: 0,
        })
    }
}

impl<B: Backend> UiView<B> for MainView {
    fn events_loop(
        &mut self,
        terminal: &mut Terminal<B>,
        settings: &mut Settings,
    ) -> Result<Option<UiViewKind>, Box<dyn Error>> {
        loop {
            let draw = |f: &mut Frame<'_, B>| {
                let main_layout = Layout::default()
                    .direction(Direction::Vertical)
                    .constraints(
                        [
                            Constraint::Length(1),
                            Constraint::Min(5),
                            Constraint::Length(3),
                        ]
                        .as_ref(),
                    )
                    .split(f.size());

                let ctrl_num_style = Style::default()
                    .bg(Color::Blue)
                    .fg(Color::LightMagenta)
                    .add_modifier(Modifier::BOLD);

                let ctrl_action_style = Style::default()
                    .bg(Color::DarkGray)
                    .fg(Color::White)
                    .add_modifier(Modifier::BOLD);

                let ctrls = Spans::from(vec![
                    Span::styled(" 1", ctrl_num_style),
                    Span::styled("File    ", ctrl_action_style),
                    Span::styled(" 2", ctrl_num_style),
                    Span::styled("Domain  ", ctrl_action_style),
                    Span::styled(" 3", ctrl_num_style),
                    Span::styled("Iter    ", ctrl_action_style),
                    Span::styled(" 4", ctrl_num_style),
                    Span::styled("Trace   ", ctrl_action_style),
                ]);

                let ctrls = Paragraph::new(ctrls).style(ctrl_action_style);
                f.render_widget(ctrls, main_layout[0]);

                let prog_layout = Layout::default()
                    .direction(Direction::Horizontal)
                    .constraints([Constraint::Percentage(50), Constraint::Percentage(50)].as_ref())
                    .split(main_layout[1]);

                let stat_block = standard_block(" Program ", None);

                if let Some(stat) = &settings.stat {
                    let highlight_lab = self.trace[self.trace_idx].label;
                    let stat = stat_table(stat, highlight_lab).block(stat_block);
                    f.render_widget(stat, prog_layout[0]);
                } else {
                    let stat =
                        Paragraph::new("Please press F1 to select a program").block(stat_block);
                    f.render_widget(stat, prog_layout[0]);
                }

                let domain_layout = Layout::default()
                    .direction(Direction::Vertical)
                    .constraints([Constraint::Length(3), Constraint::Min(5)].as_ref())
                    .split(prog_layout[1]);

                let domain_titles = settings
                    .interp
                    .domain_kinds
                    .iter()
                    .map(|kind| Spans::from(kind.name()))
                    .collect();

                let tab = domains_tab(domain_titles, self.domain_idx);
                f.render_widget(tab, domain_layout[0]);

                let domain_table_block = standard_block(" Domain viewer ", None);

                if !self.trace.is_empty() && !settings.interp.domain_kinds.is_empty() {
                    let table = domains_table(
                        &self.trace[self.trace_idx].store,
                        settings.interp.domain_kinds[self.domain_idx],
                    )
                    .block(domain_table_block);
                    f.render_widget(table, domain_layout[1]);
                } else {
                    let table = Paragraph::new("").block(domain_table_block);
                    f.render_widget(table, domain_layout[1]);
                }

                if !self.trace.is_empty() {
                    let trace = &self.trace[self.trace_idx];
                    let stat = settings
                        .stat
                        .as_ref()
                        .expect("stat None but trace non empty")
                        .get_with_label(trace.label)
                        .expect("label not found");

                    let gauge = progress_bar(trace, self.trace_idx, self.trace.len(), stat);
                    f.render_widget(gauge, main_layout[2]);
                }
            };

            terminal.draw(draw).expect("drawing error");

            match io::stdin().keys().next().unwrap().unwrap() {
                Key::Ctrl('c') => break Ok(None),
                Key::F(1) => {
                    break Ok(Some(UiViewKind::File));
                }
                Key::F(2) => {
                    break Ok(Some(UiViewKind::Domain));
                }
                Key::F(3) => {
                    break Ok(Some(UiViewKind::Iter));
                }
                Key::F(4) => {
                    break Ok(Some(UiViewKind::Trace));
                }
                Key::Up => {
                    if !self.trace.is_empty() {
                        self.trace_idx = if self.trace_idx == 0 {
                            self.trace.len() - 1
                        } else {
                            self.trace_idx - 1
                        }
                    }
                }
                Key::Down => {
                    if !self.trace.is_empty() {
                        self.trace_idx = if self.trace_idx == self.trace.len() - 1 {
                            0
                        } else {
                            self.trace_idx + 1
                        }
                    }
                }
                Key::PageUp => {
                    if !self.trace.is_empty() {
                        self.trace_idx = if self.trace_idx == 0 {
                            self.trace.len() - 1
                        } else if self.trace_idx < 10 {
                            0
                        } else {
                            self.trace_idx - 10
                        }
                    }
                }
                Key::PageDown => {
                    if !self.trace.is_empty() {
                        self.trace_idx = if self.trace_idx == self.trace.len() - 1 {
                            0
                        } else if self.trace.len() - self.trace_idx <= 10 {
                            self.trace.len() - 1
                        } else {
                            self.trace_idx + 10
                        }
                    }
                }

                Key::Home => {
                    self.trace_idx = 0;
                }
                Key::End => {
                    if !self.trace.is_empty() {
                        self.trace_idx = self.trace.len() - 1;
                    }
                }
                Key::Left => {
                    if !settings.interp.domain_kinds.is_empty() {
                        self.domain_idx = if self.domain_idx == 0 {
                            settings.interp.domain_kinds.len() - 1
                        } else {
                            self.domain_idx - 1
                        }
                    }
                }
                Key::Right => {
                    if !settings.interp.domain_kinds.is_empty() {
                        self.domain_idx =
                            if self.domain_idx == settings.interp.domain_kinds.len() - 1 {
                                0
                            } else {
                                self.domain_idx + 1
                            }
                    }
                }
                _ => (),
            }
        }
    }
}

fn stat_table(stat: &Stat, highlight: usize) -> Table {
    fn rec(stat: &Stat, highlight: usize, lv: usize, rows: &mut Vec<Row>) {
        fn line(rows: &[Row]) -> String {
            format!("{:4}", rows.len())
        }

        let style = |label| {
            if highlight == label {
                Style::default().fg(Color::Green)
            } else {
                Style::default()
            }
        };

        match stat {
            Stat::Assign(v, a, label) => {
                let code = format!("{}v{} := {}", TAB_STR.repeat(lv), v, a);

                rows.push(
                    Row::new(vec![
                        Cell::from(line(rows)),
                        Cell::from(format!("l{}", label))
                            .style(style(*label).add_modifier(Modifier::BOLD)),
                        Cell::from(code),
                    ])
                    .style(style(*label)),
                );
            }
            Stat::Skip(label) => {
                let code = format!("{}skip", TAB_STR.repeat(lv));

                rows.push(
                    Row::new(vec![
                        Cell::from(line(rows)),
                        Cell::from(format!("l{}", label))
                            .style(style(*label).add_modifier(Modifier::BOLD)),
                        Cell::from(code),
                    ])
                    .style(style(*label)),
                );
            }
            Stat::Concat(l, r) => {
                rec(l.as_ref(), highlight, lv, rows);
                rec(r.as_ref(), highlight, lv, rows);
            }
            Stat::IfElse(g, t, e, label) => {
                let if_header = format!("{}if {}", TAB_STR.repeat(lv), g);

                rows.push(
                    Row::new(vec![
                        Cell::from(line(rows)),
                        Cell::from(format!("l{}", label))
                            .style(style(*label).add_modifier(Modifier::BOLD)),
                        Cell::from(if_header),
                    ])
                    .style(style(*label)),
                );

                rec(t.as_ref(), highlight, lv + 1, rows);

                let else_header = format!("{}else", TAB_STR.repeat(lv));

                rows.push(
                    Row::new(vec![
                        Cell::from(line(rows)),
                        Cell::from("     ").style(style(*label).add_modifier(Modifier::BOLD)),
                        Cell::from(else_header),
                    ])
                    .style(style(*label)),
                );

                rec(e.as_ref(), highlight, lv + 1, rows);
            }
            Stat::While(g, t, bef, head) => {
                //let while_header = format!("{}while {}", TAB_STR.repeat(lv), g);
                let while_header = Spans::from(vec![
                    Span::raw(format!("{}while ", TAB_STR.repeat(lv))),
                    Span::styled(
                        format!("l{}", head),
                        style(*head).add_modifier(Modifier::BOLD),
                    ),
                    Span::raw(format!(" {} ", g)),
                ]);

                rows.push(Row::new(vec![
                    Cell::from(line(rows)),
                    Cell::from(format!("l{}", bef)).style(style(*bef).add_modifier(Modifier::BOLD)),
                    Cell::from(while_header),
                ]));

                rec(t.as_ref(), highlight, lv + 1, rows);
            }
        }
    }

    let mut rows = Vec::new();
    rec(stat, highlight, 0, &mut rows);

    Table::new(rows).widths(&[
        Constraint::Length(5),
        Constraint::Length(5),
        Constraint::Percentage(100),
    ])
}

fn domains_tab(titles: Vec<Spans<'_>>, selected: usize) -> Tabs<'_> {
    let block = standard_block(" Domain selector ", None);

    Tabs::new(titles)
        .block(block)
        .select(selected)
        .highlight_style(
            Style::default()
                .fg(Color::Yellow)
                .add_modifier(Modifier::BOLD),
        )
        .divider(DOT)
}

fn domains_table<'a>(store: &LabelStore, kind: StateDomainKind) -> Table<'a> {
    let non_rela = match kind {
        StateDomainKind::Sign | StateDomainKind::Const | StateDomainKind::Interval => true,
    };

    let (rows, header, widths) = if non_rela {
        let vars = VARS.lock().expect("mutex left locked!");
        let header = ["Label".to_owned()]
            .into_iter()
            .chain(vars.iter().map(|v| format!("v{}", v)))
            .collect::<Vec<_>>();
        let widths = iter::repeat(Constraint::Ratio(1, header.len() as u32))
            .take(header.len())
            .collect::<Vec<_>>();
        let header = Row::new(header).style(Style::default().add_modifier(Modifier::BOLD));
        let rows = store
            .iter()
            .enumerate()
            .map(|(label, dstore)| (label, NonRela::uncover(dstore.0[&kind].clone())))
            .map(|(label, domain)| {
                let mut row = vars
                    .iter()
                    .map(|v| Cell::from(format!("{}", domain[*v])))
                    .collect::<Vec<_>>();
                row.insert(0, Cell::from(format!("l{:<4}", label)));
                Row::new(row)
            })
            .collect::<Vec<_>>();

        (rows, header, widths)
    } else {
        let header = vec!["Label", "Domain"];
        let widths = vec![Constraint::Percentage(50), Constraint::Percentage(50)];
        let header = Row::new(header).style(Style::default().add_modifier(Modifier::BOLD));
        let rows = store
            .iter()
            .enumerate()
            .map(|(label, dstore)| (label, &dstore.0[&kind]))
            .map(|(label, domain)| {
                Row::new(vec![format!("l{:<4}", label), format!("{:?}", domain)])
            })
            .collect::<Vec<_>>();

        (rows, header, widths)
    };

    Table::new(rows).header(header).widths(widths.leak())
}

fn progress_bar<'a>(trace: &'a TracePoint, idx: usize, len: usize, _stat: &'a Stat) -> Gauge<'a> {
    let len_size = format!("{}", len - 1).len();
    let perc = (idx as f32 / (len - 1) as f32 * 100f32) as u16;

    let mut label = format!("[{:2$}/{:2$}]", idx, len - 1, len_size);

    match trace.kind {
        TracePointKind::Assign => {
            label += " assignment";
        }
        TracePointKind::Skip => {
            label += " skip";
        }
        TracePointKind::IfElse => {
            label += " filtering";
        }
        TracePointKind::WhileIncoming => {
            label += " loop incoming";
        }
        TracePointKind::WhileJoin => {
            label += " loop join";
        }
        TracePointKind::WhileWiden => {
            label += " loop widening";
        }
        TracePointKind::WhileMeet => {
            label += " loop meet";
        }
        TracePointKind::WhileNarrow => {
            label += " loop narrowing";
        }
    }

    Gauge::default()
        .block(Block::default().borders(Borders::ALL))
        .gauge_style(
            Style::default()
                .fg(Color::White)
                .bg(Color::Black)
                .add_modifier(Modifier::ITALIC),
        )
        .label(label)
        .percent(perc)
}

use std::collections::{BTreeMap, HashMap};
use std::error::Error;
use std::io;

use termion::event::Key;
use termion::input::TermRead;
use tui::backend::Backend;
use tui::layout::{Alignment, Constraint, Direction, Layout};
use tui::style::{Color, Modifier, Style};
use tui::terminal::Terminal;
use tui::text::{Span, Spans};
use tui::widgets::Paragraph;
use tui::Frame;

use crate::ast::VarId;
use crate::parser::parse_init_value;
use crate::ui::{standard_block, Settings, UiView, UiViewKind};
use crate::VARS;

const INIT_TEXT_LEN: usize = 20;

pub struct InitView {
    selection: VarId,
    cur_cursor: usize,
    cur_text: BTreeMap<VarId, String>,
}

impl InitView {
    pub fn from_settings(settings: &Settings) -> Self {
        assert!(settings.stat.is_some());

        let cur_text: BTreeMap<_, _> = VARS
            .lock()
            .expect("mutex left locked!")
            .iter()
            .map(|v| (*v, " ".repeat(INIT_TEXT_LEN)))
            .collect();

        let selection = cur_text
            .keys()
            .next()
            .copied()
            .expect("empty map of variables");

        InitView {
            selection,
            cur_cursor: 0,
            cur_text,
        }
    }

    fn get_text(&self, var: VarId) -> Spans {
        Spans::from(
            (0..INIT_TEXT_LEN)
                .into_iter()
                .map(|i| {
                    let selected = var == self.selection;
                    let hover = i == self.cur_cursor;

                    let style = match (selected, hover) {
                        (true, true) => Style::default().bg(Color::Red),
                        (true, false) => Style::default().bg(Color::Blue),
                        (false, true) => Style::default().bg(Color::DarkGray),
                        (false, false) => Style::default().bg(Color::DarkGray),
                    };

                    let text = self.cur_text[&var]
                        .chars()
                        .nth(i)
                        .expect("too short cur_text")
                        .to_string();

                    Span::styled(text, style)
                })
                .collect::<Vec<_>>(),
        )
    }

    fn set_char(&mut self, c: char) {
        *self.cur_text.get_mut(&self.selection).unwrap() = self.cur_text[&self.selection]
            .chars()
            .enumerate()
            .map(|(i, d)| if i == self.cur_cursor { c } else { d })
            .collect::<String>();
    }

    fn next_var(&self, cur: VarId) -> VarId {
        let next = self.cur_text.keys().skip_while(|v| **v != cur).nth(1);

        match next {
            Some(next) => *next,
            None => {
                // `cur` is the last variable, hence return the
                // first one.
                *self.cur_text.keys().next().expect("empty map of variables")
            }
        }
    }

    fn prev_var(&self, cur: VarId) -> VarId {
        let next = self.cur_text.keys().rev().skip_while(|v| **v != cur).nth(1);

        match next {
            Some(next) => *next,
            None => {
                // `cur` is the first variable, hence return the
                // last one.
                *self
                    .cur_text
                    .keys()
                    .rev()
                    .next()
                    .expect("empty map of variables")
            }
        }
    }
}

impl<B: Backend> UiView<B> for InitView {
    fn events_loop(
        &mut self,
        terminal: &mut Terminal<B>,
        settings: &mut Settings,
    ) -> Result<Option<UiViewKind>, Box<dyn Error>> {
        loop {
            let draw = |f: &mut Frame<'_, B>| {
                let main_layout = Layout::default()
                    .direction(Direction::Vertical)
                    .constraints([Constraint::Length(1), Constraint::Min(5)].as_ref())
                    .split(f.size());

                let ctrl_num_style = Style::default()
                    .bg(Color::Blue)
                    .fg(Color::LightMagenta)
                    .add_modifier(Modifier::BOLD);

                let ctrl_action_style = Style::default()
                    .bg(Color::DarkGray)
                    .fg(Color::White)
                    .add_modifier(Modifier::BOLD);

                let ctrls = Spans::from(vec![
                    Span::styled(" 1", ctrl_num_style),
                    Span::styled("OK      ", ctrl_action_style),
                    Span::styled(" 2", ctrl_num_style),
                    Span::styled("Cancel  ", ctrl_action_style),
                ]);

                let ctrls = Paragraph::new(ctrls).style(ctrl_action_style);
                f.render_widget(ctrls, main_layout[0]);

                let init_block = standard_block(" Initialization values ", None);

                let mut constrs = self
                    .cur_text
                    .iter()
                    .map(|_| Constraint::Length(3))
                    .collect::<Vec<_>>();
                constrs.push(Constraint::Min(1));

                let vars_layout = Layout::default()
                    .direction(Direction::Vertical)
                    .constraints(constrs)
                    .split(init_block.inner(main_layout[1]));

                f.render_widget(init_block, main_layout[1]);

                for (i, var) in self.cur_text.keys().enumerate() {
                    let v_str = format!(" v{} ", var);
                    let para = Paragraph::new(self.get_text(*var))
                        .block(standard_block(&v_str, Some(Alignment::Left)));
                    f.render_widget(para, vars_layout[i]);
                }
            };

            terminal.draw(draw).expect("drawing error");

            match io::stdin().keys().next().unwrap().unwrap() {
                Key::Ctrl('c') => break Ok(None),
                Key::F(1) | Key::Char('\n') => {
                    let init: Result<HashMap<_, _>, _> = self
                        .cur_text
                        .iter()
                        .map(|(var, txt)| {
                            parse_init_value(txt.as_bytes()).map(|intrv| (*var, intrv))
                        })
                        .collect();
                    settings.interp.init = init?;

                    break Ok(Some(UiViewKind::Main));
                }
                Key::F(2) | Key::Esc => {
                    break Ok(Some(UiViewKind::Main));
                }
                Key::Down => {
                    self.selection = self.next_var(self.selection);
                    self.cur_cursor = 0;
                }
                Key::Up => {
                    self.selection = self.prev_var(self.selection);
                    self.cur_cursor = 0;
                }
                Key::Right => {
                    self.cur_cursor = (self.cur_cursor + 1) % INIT_TEXT_LEN;
                }
                Key::Left => {
                    self.cur_cursor = (self.cur_cursor + INIT_TEXT_LEN - 1) % INIT_TEXT_LEN;
                }
                Key::Char(c) => {
                    self.set_char(c);
                    self.cur_cursor = (self.cur_cursor + 1) % INIT_TEXT_LEN;
                }
                Key::Backspace => {
                    self.cur_cursor = (self.cur_cursor + INIT_TEXT_LEN - 1) % INIT_TEXT_LEN;
                    self.set_char(' ');
                }
                _ => (),
            }
        }
    }
}

use std::error::Error;
use std::io;
use std::str::FromStr;

use termion::event::Key;
use termion::input::TermRead;
use tui::backend::Backend;
use tui::layout::{Alignment, Constraint, Direction, Layout};
use tui::style::{Color, Modifier, Style};
use tui::terminal::Terminal;
use tui::text::{Span, Spans};
use tui::widgets::Paragraph;
use tui::Frame;

use crate::ui::{standard_block, Settings, UiView, UiViewKind};

pub struct IterView {
    selection: IterField,
    cur_cursor: usize,
    cur_text: [String; 3],
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum IterField {
    Join,
    Meet,
    Narrow,
}

impl IterField {
    fn next(&self) -> Self {
        match self {
            Self::Join => Self::Meet,
            Self::Meet => Self::Narrow,
            Self::Narrow => Self::Join,
        }
    }

    fn prev(&self) -> Self {
        match self {
            Self::Join => Self::Narrow,
            Self::Meet => Self::Join,
            Self::Narrow => Self::Meet,
        }
    }

    fn idx(&self) -> usize {
        match self {
            Self::Join => 0,
            Self::Meet => 1,
            Self::Narrow => 2,
        }
    }
}

impl IterView {
    pub fn from_settings(settings: &Settings) -> Self {
        let cur_text = [
            format!("{:<4}", settings.interp.join_iter),
            format!("{:<4}", settings.interp.meet_iter),
            settings
                .interp
                .narrow_iter
                .map(|i| format!("{:<4}", i))
                .unwrap_or_else(|| "inf ".to_owned()),
        ];

        IterView {
            selection: IterField::Join,
            cur_cursor: 0,
            cur_text,
        }
    }

    fn get_text(&self, field: IterField) -> Spans {
        Spans::from(
            (0..4)
                .into_iter()
                .map(|i| {
                    let selected = field == self.selection;
                    let hover = i == self.cur_cursor;

                    let style = match (selected, hover) {
                        (true, true) => Style::default().bg(Color::Red),
                        (true, false) => Style::default().bg(Color::Blue),
                        (false, true) => Style::default().bg(Color::DarkGray),
                        (false, false) => Style::default().bg(Color::DarkGray),
                    };

                    let text = self.cur_text[field.idx()]
                        .chars()
                        .nth(i)
                        .expect("too short cur_text")
                        .to_string();

                    Span::styled(text, style)
                })
                .collect::<Vec<_>>(),
        )
    }

    fn set_char(&mut self, c: char) {
        let idx = self.selection.idx();
        self.cur_text[idx] = self.cur_text[idx]
            .chars()
            .enumerate()
            .map(|(i, d)| if i == self.cur_cursor { c } else { d })
            .collect::<String>();
    }
}

impl<B: Backend> UiView<B> for IterView {
    fn events_loop(
        &mut self,
        terminal: &mut Terminal<B>,
        settings: &mut Settings,
    ) -> Result<Option<UiViewKind>, Box<dyn Error>> {
        loop {
            let draw =
                |f: &mut Frame<'_, B>| {
                    let main_layout = Layout::default()
                        .direction(Direction::Vertical)
                        .constraints([Constraint::Length(1), Constraint::Min(5)].as_ref())
                        .split(f.size());

                    let ctrl_num_style = Style::default()
                        .bg(Color::Blue)
                        .fg(Color::LightMagenta)
                        .add_modifier(Modifier::BOLD);

                    let ctrl_action_style = Style::default()
                        .bg(Color::DarkGray)
                        .fg(Color::White)
                        .add_modifier(Modifier::BOLD);

                    let ctrls = Spans::from(vec![
                        Span::styled(" 1", ctrl_num_style),
                        Span::styled("OK      ", ctrl_action_style),
                        Span::styled(" 2", ctrl_num_style),
                        Span::styled("Cancel  ", ctrl_action_style),
                    ]);

                    let ctrls = Paragraph::new(ctrls).style(ctrl_action_style);
                    f.render_widget(ctrls, main_layout[0]);

                    let iter_block = standard_block(" Iterator settings ", None);

                    let settings_layout = Layout::default()
                        .direction(Direction::Vertical)
                        .constraints([
                            Constraint::Length(3),
                            Constraint::Length(3),
                            Constraint::Length(3),
                            Constraint::Min(1), // eat all the remaining space.
                        ])
                        .split(iter_block.inner(main_layout[1]));

                    f.render_widget(iter_block, main_layout[1]);

                    let union = Paragraph::new(self.get_text(IterField::Join)).block(
                        standard_block(" Number of join iterations ", Some(Alignment::Left)),
                    );
                    f.render_widget(union, settings_layout[0]);

                    let meet = Paragraph::new(self.get_text(IterField::Meet)).block(
                        standard_block(" Number of meet iterations ", Some(Alignment::Left)),
                    );
                    f.render_widget(meet, settings_layout[1]);

                    let narrow =
                        Paragraph::new(self.get_text(IterField::Narrow)).block(standard_block(
                            " Number of narrowing iterations (type 'inf' for unlimited narrowing) ",
                            Some(Alignment::Left),
                        ));
                    f.render_widget(narrow, settings_layout[2]);
                };

            terminal.draw(draw).expect("drawing error");

            match io::stdin().keys().next().unwrap().unwrap() {
                Key::Ctrl('c') => break Ok(None),
                Key::F(1) | Key::Char('\n') => {
                    settings.interp.join_iter = usize::from_str(self.cur_text[0].trim())?;
                    settings.interp.meet_iter = usize::from_str(self.cur_text[1].trim())?;
                    settings.interp.narrow_iter = if self.cur_text[2].trim() == "inf" {
                        None
                    } else {
                        Some(usize::from_str(self.cur_text[2].trim())?)
                    };

                    break Ok(Some(UiViewKind::Main));
                }
                Key::F(2) | Key::Esc => {
                    break Ok(Some(UiViewKind::Main));
                }
                Key::Down => {
                    self.selection = self.selection.next();
                    self.cur_cursor = 0;
                }
                Key::Up => {
                    self.selection = self.selection.prev();
                    self.cur_cursor = 0;
                }
                Key::Right => {
                    self.cur_cursor = (self.cur_cursor + 1) % 4;
                }
                Key::Left => {
                    self.cur_cursor = (self.cur_cursor + 4 - 1) % 4;
                }
                Key::Char(c) => {
                    self.set_char(c);
                    self.cur_cursor = (self.cur_cursor + 1) % 4;
                }
                Key::Backspace => {
                    self.cur_cursor = (self.cur_cursor + 4 - 1) % 4;
                    self.set_char(' ');
                }
                _ => (),
            }
        }
    }
}

use std::error::Error;
use std::io;

use termion::event::Key;
use termion::input::TermRead;
use tui::backend::Backend;
use tui::layout::{Constraint, Direction, Layout};
use tui::style::{Color, Modifier, Style};
use tui::terminal::Terminal;
use tui::text::{Span, Spans};
use tui::widgets::{Paragraph, Wrap};
use tui::Frame;

use crate::ui::{standard_block, Settings, UiView, UiViewKind};

pub struct ErrorView {
    error: Box<dyn Error>,
}

impl ErrorView {
    pub fn from_error(error: Box<dyn Error>) -> Self {
        ErrorView { error }
    }
}

impl<B: Backend> UiView<B> for ErrorView {
    fn events_loop(
        &mut self,
        terminal: &mut Terminal<B>,
        _settings: &mut Settings,
    ) -> Result<Option<UiViewKind>, Box<dyn Error>> {
        loop {
            let draw = |f: &mut Frame<'_, B>| {
                let main_layout = Layout::default()
                    .direction(Direction::Vertical)
                    .constraints([Constraint::Length(1), Constraint::Min(1)].as_ref())
                    .split(f.size());

                let ctrl_num_style = Style::default()
                    .bg(Color::Blue)
                    .fg(Color::LightMagenta)
                    .add_modifier(Modifier::BOLD);

                let ctrl_action_style = Style::default()
                    .bg(Color::DarkGray)
                    .fg(Color::White)
                    .add_modifier(Modifier::BOLD);

                let ctrls = Spans::from(vec![
                    Span::styled(" 1", ctrl_num_style),
                    Span::styled("Continue", ctrl_action_style),
                ]);

                let ctrls = Paragraph::new(ctrls).style(ctrl_action_style);
                f.render_widget(ctrls, main_layout[0]);

                let error = Paragraph::new(format!("{}", self.error))
                    .style(Style::default().bg(Color::Red))
                    .block(standard_block(" Error ", None))
                    .wrap(Wrap { trim: true });
                f.render_widget(error, main_layout[1]);
            };

            terminal.draw(draw).expect("drawing error");

            match io::stdin().keys().next().unwrap().unwrap() {
                Key::Ctrl('c') => break Ok(None),
                Key::F(1) | Key::Char('\n') | Key::Esc => {
                    break Ok(Some(UiViewKind::Main));
                }
                _ => (),
            }
        }
    }
}

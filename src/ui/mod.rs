//! This module implements the UI.
//!
//! This application implements a Terminal User Interface (TUI) based
//! on the library [tui-rs](https://github.com/fdehau/tui-rs). The
//! interface provides 6 different windows (here called *views*):
//!   * The **main view:** it contains the program and the domain viewer.
//!   * The **file picker view**.
//!   * The **domain view:** it allows the user to select which abstract
//!   domain to use for the analysis.
//!   * The **iterator configuration view:** it allows the user to configure
//!   the number of iterations that the interpreter should run in the
//!   analysis of while loops.
//!   * The **tracepoint view:** it allows the user user to select which
//!   kind of tracepoints to collect during the analysis.
//!   * The **initializatio view:** it allows the user to set the initial
//!   state of the program.
//!   * The **error view:** it appears when the application faces a runtime
//!   error (e.g. parser failure).
//!
//! The events loop and the UI initialization is handled by the `events_loop` function.

use std::default::Default;
use std::error::Error;

use tui::backend::Backend;
use tui::layout::Alignment;
use tui::style::{Modifier, Style};
use tui::terminal::Terminal;
use tui::text::Span;
use tui::widgets::{Block, BorderType, Borders, ListState};

mod domain;
mod error;
mod file;
mod init;
mod iter;
mod main;
mod trace;

use crate::ast::Stat;
use crate::interp::InterpSettings;

use domain::DomainView;
use error::ErrorView;
use file::FileView;
use init::InitView;
use iter::IterView;
use main::MainView;
use trace::TraceView;

fn standard_block(text: &str, align: Option<Alignment>) -> Block {
    Block::default()
        .title(Span::styled(
            text,
            Style::default().add_modifier(Modifier::BOLD),
        ))
        .title_alignment(align.unwrap_or(Alignment::Center))
        .borders(Borders::ALL)
        .border_type(BorderType::Rounded)
}

fn list_up(state: &mut ListState, max: usize, step: usize) {
    let i = match state.selected() {
        Some(i) => {
            if i == 0 {
                max - 1
            } else if i < step {
                0
            } else {
                i - step
            }
        }
        None => 0,
    };

    state.select(Some(i));
}

fn list_down(state: &mut ListState, max: usize, step: usize) {
    let i = match state.selected() {
        Some(i) => {
            if i == max - 1 {
                0
            } else if i + step >= max {
                max - 1
            } else {
                i + step
            }
        }
        None => 0,
    };

    state.select(Some(i));
}

#[derive(Default, Debug, Clone)]
pub struct Settings {
    stat: Option<Stat>,
    interp: InterpSettings,
}

#[derive(Debug)]
enum UiViewKind {
    Main,
    File,
    Domain,
    Init,
    Iter,
    Trace,
}

trait UiView<B: Backend> {
    fn events_loop(
        &mut self,
        terminal: &mut Terminal<B>,
        settings: &mut Settings,
    ) -> Result<Option<UiViewKind>, Box<dyn Error>>;
}

fn run_view<B: Backend>(
    kind: UiViewKind,
    terminal: &mut Terminal<B>,
    settings: &mut Settings,
) -> Result<Option<UiViewKind>, Box<dyn Error>> {
    let mut ui_view: Box<dyn UiView<B>> = match kind {
        UiViewKind::Main => Box::new(MainView::from_settings(settings)?),
        UiViewKind::File => Box::new(FileView::from_settings(settings)?),
        UiViewKind::Domain => Box::new(DomainView::from_settings(settings)),
        UiViewKind::Iter => Box::new(IterView::from_settings(settings)),
        UiViewKind::Init => Box::new(InitView::from_settings(settings)),
        UiViewKind::Trace => Box::new(TraceView::from_settings(settings)),
    };

    ui_view.events_loop(terminal, settings)
}

/// Entry point of the user interface.
///
/// This function does not return until the user wants to quit the program.
pub fn events_loop<B: Backend>(terminal: &mut Terminal<B>) -> Result<(), Box<dyn Error>> {
    let mut settings = Settings::default();
    let mut view_kind = UiViewKind::Main;

    terminal.clear()?;

    loop {
        match run_view(view_kind, terminal, &mut settings) {
            Ok(Some(next)) => view_kind = next,
            Ok(None) => break Ok(()),
            Err(error) => {
                let next = ErrorView::from_error(error).events_loop(terminal, &mut settings)?;

                match next {
                    Some(next) => {
                        view_kind = next;
                        settings = Settings::default();
                    }
                    None => break Ok(()),
                }
            }
        }
    }
}

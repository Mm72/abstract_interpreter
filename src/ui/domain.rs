use std::error::Error;
use std::io;

use termion::event::Key;
use termion::input::TermRead;
use tui::backend::Backend;
use tui::layout::{Alignment, Constraint, Direction, Layout};
use tui::style::{Color, Modifier, Style};
use tui::terminal::Terminal;
use tui::text::{Span, Spans};
use tui::widgets::{Block, BorderType, Borders, List, ListItem, ListState, Paragraph};
use tui::Frame;

use crate::domain::DOMAINS;
use crate::ui::{list_down, list_up, Settings, UiView, UiViewKind};

pub struct DomainView {
    lst_state: ListState,
    enabled: Vec<bool>,
}

impl DomainView {
    pub fn from_settings(settings: &Settings) -> Self {
        let enabled = DOMAINS
            .iter()
            .map(|kind| settings.interp.domain_kinds.contains(kind))
            .collect();
        let mut lst_state = ListState::default();
        lst_state.select(Some(0));

        DomainView { lst_state, enabled }
    }
}

impl<B: Backend> UiView<B> for DomainView {
    fn events_loop(
        &mut self,
        terminal: &mut Terminal<B>,
        settings: &mut Settings,
    ) -> Result<Option<UiViewKind>, Box<dyn Error>> {
        loop {
            let draw = |f: &mut Frame<'_, B>| {
                let main_layout = Layout::default()
                    .direction(Direction::Vertical)
                    .constraints([Constraint::Length(1), Constraint::Min(5)].as_ref())
                    .split(f.size());

                let ctrl_num_style = Style::default()
                    .bg(Color::Blue)
                    .fg(Color::LightMagenta)
                    .add_modifier(Modifier::BOLD);

                let ctrl_action_style = Style::default()
                    .bg(Color::DarkGray)
                    .fg(Color::White)
                    .add_modifier(Modifier::BOLD);

                let ctrls = Spans::from(vec![
                    Span::styled(" 1", ctrl_num_style),
                    Span::styled("OK      ", ctrl_action_style),
                    Span::styled(" 2", ctrl_num_style),
                    Span::styled("Cancel  ", ctrl_action_style),
                ]);

                let ctrls = Paragraph::new(ctrls).style(ctrl_action_style);
                f.render_widget(ctrls, main_layout[0]);

                let items = DOMAINS
                    .iter()
                    .enumerate()
                    .map(|(i, kind)| {
                        let enabled = self.enabled[i];
                        let selected = self.lst_state.selected().expect("no selection") == i;

                        let style = match (enabled, selected) {
                            (true, true) => Style::default()
                                .add_modifier(Modifier::BOLD)
                                .fg(Color::Yellow),
                            (true, false) => Style::default().add_modifier(Modifier::BOLD),
                            (false, true) => Style::default().fg(Color::Yellow),
                            (false, false) => Style::default(),
                        };

                        ListItem::new(Span::styled(kind.name(), style))
                    })
                    .collect::<Vec<_>>();

                let list_block = Block::default()
                    .title(Span::styled(
                        " Select the abstract domains ",
                        Style::default().add_modifier(Modifier::BOLD),
                    ))
                    .title_alignment(Alignment::Center)
                    .borders(Borders::ALL)
                    .border_type(BorderType::Rounded);

                let list = List::new(items).block(list_block);
                f.render_stateful_widget(list, main_layout[1], &mut self.lst_state);
            };

            terminal.draw(draw).expect("drawing error");

            match io::stdin().keys().next().unwrap().unwrap() {
                Key::Ctrl('c') => break Ok(None),
                Key::F(1) | Key::Char('\n') => {
                    settings.interp.domain_kinds = DOMAINS
                        .iter()
                        .enumerate()
                        .filter_map(|(i, kind)| if self.enabled[i] { Some(*kind) } else { None })
                        .collect();

                    break Ok(Some(UiViewKind::Main));
                }
                Key::F(2) | Key::Esc => {
                    break Ok(Some(UiViewKind::Main));
                }
                Key::Down => {
                    list_down(&mut self.lst_state, DOMAINS.len(), 1);
                }
                Key::Up => {
                    list_up(&mut self.lst_state, DOMAINS.len(), 1);
                }
                Key::Char(' ') => match self.lst_state.selected() {
                    Some(idx) => self.enabled[idx] = !self.enabled[idx],
                    None => unreachable!(),
                },
                _ => (),
            }
        }
    }
}

use std::error::Error;
use std::fs::{read_dir, File};
use std::io;
use std::io::BufReader;
use std::path::PathBuf;

use termion::event::Key;
use termion::input::TermRead;
use tui::backend::Backend;
use tui::layout::{Constraint, Direction, Layout};
use tui::style::{Color, Modifier, Style};
use tui::terminal::Terminal;
use tui::text::{Span, Spans};
use tui::widgets::{List, ListItem, ListState, Paragraph};
use tui::Frame;

use crate::parser::parse_src;
use crate::ui::{list_down, list_up, standard_block, Settings, UiView, UiViewKind};
use crate::VARS;

pub struct FileView {
    files: Vec<(String, PathBuf)>,
    lst_state: ListState,
}

impl FileView {
    pub fn from_settings(_settings: &Settings) -> Result<Self, Box<dyn Error>> {
        let mut files = Vec::new();

        for entry in read_dir("examples")? {
            let entry = entry?;
            files.push((
                entry
                    .file_name()
                    .into_string()
                    .expect("invalid chars in file name"),
                entry.path(),
            ));
        }

        files.sort_unstable();

        let mut lst_state = ListState::default();
        lst_state.select(Some(0));

        Ok(FileView { files, lst_state })
    }
}

impl<B: Backend> UiView<B> for FileView {
    fn events_loop(
        &mut self,
        terminal: &mut Terminal<B>,
        settings: &mut Settings,
    ) -> Result<Option<UiViewKind>, Box<dyn Error>> {
        loop {
            let draw = |f: &mut Frame<'_, B>| {
                let main_layout = Layout::default()
                    .direction(Direction::Vertical)
                    .constraints([Constraint::Length(1), Constraint::Min(5)].as_ref())
                    .split(f.size());

                let ctrl_num_style = Style::default()
                    .bg(Color::Blue)
                    .fg(Color::LightMagenta)
                    .add_modifier(Modifier::BOLD);

                let ctrl_action_style = Style::default()
                    .bg(Color::DarkGray)
                    .fg(Color::White)
                    .add_modifier(Modifier::BOLD);

                let ctrls = Spans::from(vec![
                    Span::styled(" 1", ctrl_num_style),
                    Span::styled("OK      ", ctrl_action_style),
                    Span::styled(" 2", ctrl_num_style),
                    Span::styled("Cancel  ", ctrl_action_style),
                ]);

                let ctrls = Paragraph::new(ctrls).style(ctrl_action_style);
                f.render_widget(ctrls, main_layout[0]);

                let items = self
                    .files
                    .iter()
                    .map(|(name, _)| ListItem::new(name.as_str()))
                    .collect::<Vec<_>>();

                let list_block = standard_block(" Select a program ", None);

                let list = List::new(items)
                    .block(list_block)
                    .style(Style::default().fg(Color::White))
                    .highlight_style(
                        Style::default()
                            .add_modifier(Modifier::BOLD)
                            .bg(Color::LightBlue),
                    );
                f.render_stateful_widget(list, main_layout[1], &mut self.lst_state);
            };

            terminal.draw(draw).expect("drawing error");

            match io::stdin().keys().next().unwrap().unwrap() {
                Key::Ctrl('c') => break Ok(None),
                Key::F(1) | Key::Char('\n') => {
                    let (_, path) = &self.files[self.lst_state.selected().expect("no selection")];

                    let src_f = File::open(path)?;
                    let reader = BufReader::new(src_f);
                    let stat = parse_src(reader)?;

                    {
                        let mut vars = VARS.lock().expect("mutex left locked!");
                        vars.clear();

                        for v in stat.free_vars() {
                            vars.push(v);
                        }
                    }

                    settings.stat = Some(stat);

                    break Ok(Some(UiViewKind::Init));
                }
                Key::F(2) | Key::Esc => {
                    break Ok(Some(UiViewKind::Main));
                }
                Key::Down => {
                    list_down(&mut self.lst_state, self.files.len(), 1);
                }
                Key::Up => {
                    list_up(&mut self.lst_state, self.files.len(), 1);
                }
                Key::PageDown => {
                    list_down(&mut self.lst_state, self.files.len(), 10);
                }
                Key::PageUp => {
                    list_up(&mut self.lst_state, self.files.len(), 10);
                }
                _ => (),
            }
        }
    }
}
